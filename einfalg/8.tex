\setcounter{lecture}{8}
\begin{lem}\label{8:orders}
 Let $G$ be a group, $g\in G$ an element $\ord(g)=n$. Then
 \begin{enumerate}
  \item Assume $g^m=1_G$ for an $m\in \zz$. Then $n\divd m$.
  \item Let $m\in \zz$ with $m\divd n$. Then $\ord(g^m)=n/m$
 \end{enumerate}

\end{lem}
\begin{proof}
  \begin{enumerate}
    \item Let $g^m=1$. Then $m\geq n$. Euclidean division by $n$ gives $q,r\in \zz$ with $r<n$, such that $m = qn + r$. But then
    \[
    1 = g^m = g^{qn+r}= g^{qn}\cdot g^r \implies g^r = 1,
    \] which implies $r=0$.
    \item $(g^m)^{n/m}=g^n=1$ holds. In addition, $n/m$ is the smallest integer such that $m\cdot n/m=n$ holds.
    \end{enumerate}
\end{proof}


\begin{thm}
  Let $G=\langle g\rangle$ be a cyclic group of order $n$. For a divisor $d$ of $n$, consider the subgroup $G_d:= \langle g^{n/d}\rangle$.
 \begin{enumerate}
  \item The order of these subgroups is given by $\lvert G_d\rvert = d$
  \item There are nested sequences of subgroups for divisors, i.e. $G_d \leq G_e \Longleftrightarrow d\divd e$
  \item There is a bijection
   \[
   \begin{tikzcd}
     \{1\leq d\leq n\ssp d\divd m \}\ar{r}&\{\text{subgroups of }G\}\\
     d\ar[mapsto]{r}&G_d:= \langle g^{n/d} \rangle
   \end{tikzcd}
   \]
\end{enumerate}
\end{thm}
\begin{proof}
  \begin{enumerate}
    \item This is \cref{8:orders} ii).
    \item Assume that $g\divd e$. Then there is $r\in \zz_{\geq 0}$, such that $dr = e$, so
    \[
    \left(g^{n/e}\right)^r = g^{n/d} = 1_G,
    \]
    so $G_d \subseteq G_e$.\par
    Assume that $G_d\leq G_e$. Then Lagranges Theorem implies $d\divd e$.
    \item Injectivity of the map follows from ii). For the surjectivity, we use that every subgroup of a cyclic group is again cyclic. Now let $\langle g^d\rangle \leq G$ be a subgroup and assume that $d\nmid n$. Let $d' := \ord(g^d)$, then $d'\divd n$, and $\ord(g^{d'})=n/d'$. But as $\left(g^{d'}\right)^d=1_G$, $d\divd (n/d')$ holds, and so $d\divd n$.
    \end{enumerate}
\end{proof}

\begin{defn}
  The function
  \[
  \pphi:\nn_{\geq 1}\to \nn_{\geq 1},~n \mapsto \lvert \{1\leq d\leq n\ssp \gcd(d,n) = 1\}\rvert
  \]
  is called the \toidx{Eulerian $\pphi$-function}.
\end{defn}

Recall from Linear Algebra 1: The group $\zz/n\zz$ has also the structure of a commutative ring, as ($n\zz$ is an ideal in $\zz$).
\begin{defn}
  Let $A$ be a ring. Then the set of units of $A$ has a group structure. This group is called the \emph{group of units} of $A$ and is denoted by $A^{\times}$.
\end{defn}

\begin{lem} \label{8: generators of znz}
  For $n\geq 1$, the relation
  \[
  \pphi(n) = \lvert \{ \text{generators of }\zz/n\zz\}\rvert = \lvert(\zz/n\zz)^{\times}\rvert,
  \]
  holds.
\end{lem}


\begin{proof}
  Consider the surjective ring homomorphism
  \[
  \overline{\cdot}:\zz \to \zz/n\zz,~a\mapsto \overline{a}.
  \]
  Now let $a\in \zz$. Then $\overline{a}$ generates $\zz/n\zz$ if and only if $(\overline{a})^b=\overline{1}$ for some $b\in \zz$.
\end{proof}

\begin{cor}
  Let $n\geq 1$ and $a\in \zz$. Then the following are equaivalent:
  \begin{enumerate}
    \item $\overline{a}$ is a generator of $\zz/n\zz$.
    \item $\gcd(a,n)=1$.
  \end{enumerate}
\end{cor}
\begin{lem}
  For $n\geq 1$, the map
  \[
  \pphi: \aut(\zz/n\zz)\to (\zz/n\zz)^{\times},~f\mapsto f(\overline{1})
  \]
  is an isomorphism of groups.
\end{lem}
\begin{proof}
  We first show that $\pphi:\aut(\zz/n\zz)\to \zz/n\zz$ is a group homomorphism. Let $f,g\in \aut(\nzz)$ and let $f(\rstk{1})= \rstk{m} = m\rstk{1},~g(\rstk{1})= \rstk{n}=n\rstk{1}$. Then
  \[
  f(\rstk{1})g(\rstk{1}) = (m\rstk{1})(n\rstk{1}) = mn \rstk{1}
  \]
  and
  \[
  f(g(\rstk{1})) = f(n\rstk{1})=nf(\rstk{1}) = nm\rstk{1}
  \]
  so $\pphi$ is indeed a group homomorphism.\\
  This implies that the restriction of the codomain of $\pphi$ to $\unts{\nzz}$ is well-defined, as
  \[
  f^{-1}(\rstk{1})f(\rstk{1}) = \pphi(1_{\aut(\nzz)}) = \overline{1}.
  \]
  Any group homomorphism $f:\nzz \to \nzz$ is uniquely determined by its value on $\rstk{1}$, as
  \[
  f(\rstk{m}) = f(m\rstk{1}) = mf(\rstk{1})
  \]
  holds. In particular, this implies that $\pphi$ is injective, as its kernel is trivial.\\
  Consider now for a fixed $\rstk{a}\in \unts{\nzz}$ the map
  \[
  f_a:\nzz \to \nzz,~ \rstk{b}\mapsto b \overline{a}
  \]
  Then $f_a$ is a group homomorphism, as
  \[
  f_a(\rstk{b}+\rstk{c}) = (b+c)\rstk{a} = f_a(\rstk{b})+f_a(\rstk{c}).
  \]
  In addition, $f_a$ is bijecitve, as its inverse is given by $f_{a^{-1}}$.
\end{proof}

\begin{lem}\label{8:gcdord}
  Let $G$ be an abelian group, and $g,h\in G$ elements with finite order, such that $\gcd(\ord(g),\ord(h))=1$. Then $\ord(gh)=\ord(g)\ord(h)$.
\end{lem}

\begin{lem}
  Let $G$ be a finite abelian group, and
  \[
  m:= \max \{ \ord(g)\ssp g\in G\}.
  \]
  Then for all $g\in G$, $\ord(g)\divd m$ holds.
\end{lem}
\begin{proof}
  Let $g\in G$ be any element, and suppose that $\ord(g)\nmid m$. Consider the prime decomposition
  \[
  \ord(g) = \prod_{p\text{ prime}}p^{\mu_p}\qquad \text{and}\qquad m = \prod_{p\text{ prime}}p^{\nu_p},
  \]
  for some integer $\mu_p,\nu_p$. Then there is a prime $p$ such that $\mu_p > \nu_p$. We now write
  \[
  \ord(g)= p^{\mu_p}\cdot n_0\text{ and }m = p^{\nu_p}\cdot m_0
  \]
  for some $n_0,m_0\geq 1$. By \cref{8:orders},
  \[
  \ord(g^{n_0})= p^{\mu_p}\text{ and }\ord(h^{\nu_p})=m_0
  \]
  holds, for any $h\in G$ with $\ord(h)=m$. As $\gcd(p^{\mu_p},m_0)=1$, \cref{8:gcdord} implies that
  \[
  \ord(g^{n_0}h^{\nu_p}) = p^{\mu_p}m_0 > p^{\nu_p}m_0 = m
  \]
  in contradiction to the maximality of $m$.
\end{proof}

\begin{thm}\label{8:cycunits}
Let $K$ be a field, and $G$ a finite subgroup of $K^\times$. Then $G$ is cyclic.
\end{thm}
\begin{proof}
  Let $m:= \max\{\ord{g}\ssp g\in G\}$, and $h\in G$ with $\ord(h) = m$. Then $h$ is a zero of the polynomial $X^m-1\in K[X]$. For any $g\in G$, the previous lemma implies $\ord(g)\divd m$, and in particular, $g$ is also a zero of $X^m-1$. But from Linear Algebra, we know that $X^m -1$ has only finitely many roots, so $|G|\leq m = \ord(h)\leq |G|$, hence $G = \langle h\rangle$.
\end{proof}
\begin{ncor}
  Let $p$ be prime. Then $\abs{\ff_p} \cong \zz_{p-1}$.
\end{ncor}
\section{Group Actions}
Let $G$ be a group and $X\neq \emptyset$ a set.
\begin{defn}
 A map
 \[
 \phi: G\times X\to X,~(g,x)\mapsto g.x
 \]
 is  an \emph{action of $G$ on $X$} \index{group action} if
 \begin{itemize}
  \item (G1): $1.x=x$ for all $x\in X$
  \item (G2): $g.(h.x)=(gh).x$ for all $g,h\in G$ and $x\in X$.
  \end{itemize}
  \end{defn}
\begin{rem}
  Let $\phi$ be a group action of $G$ on $X$. Define a map
  \[
  \phi':G\to \aut(X),~g\mapsto[\eta_g: x\mapsto g.x].
  \]
  Then $\phi'$ is a group homomorphism. In addition, the assignment $\phi\to \phi'$ leads to a bijection
  \[
  \begin{tikzcd}
  \{\text{group actions }G\times X\to X\} \ar[leftrightarrow]{d}\\
  \{\text{group homomorphisms }G\to S_x\}
  \end{tikzcd}
  \]
  The map $\eta_g:X\to X,~x\mapsto g.x$ is indeed bijective, as
  \[
  \eta_g\circ \eta_{g^{-1}}(x)= g.(g^{-1}.x)\overset{(G2)}{=}(gg^{-1}).x \overset{(G1)}{=}x~\text{for all }x\in X.
  \]
\end{rem}
\begin{defn}
  Let $\phi_1:G\times X_1\to X_1$ and $\phi_2:G\times X_2\to X_2$ be two group actions. A map
  \[
  f:X_1\to X_2
  \]
  is called a \emph{$G$-homomorphism} \index{homomorphism!of groups representations} if for all $g\in G$, the diagram
  \[
  \begin{tikzcd}
  X_1\ar{r}{f}\ar[swap]{d}{\eta_g}&X_2\ar{d}{\eta_g'}\\
  X_1\ar[swap]{r}{f}&X_2
  \end{tikzcd}
  \]
  commutes.
\end{defn}
\begin{defn}
Let $\pphi:G\times X\to X$ be a group action on $X$ and let $x\in X$.
\begin{enumerate}
  \item $G.x := \{g.x\ssp g\in G\}\subseteq X$ is the \toidx{orbit} of $x$.
  \item $G_x:= \{g\in G\ssp g.x=x\}\subseteq G$ is the \toidx{stabilizer} of $x$.
  \item $X^G:= \{x\in X\ssp g.x=x\text{ for all }g          \in G\}$ is the \emph{set of fixed points of $X$}.
\end{enumerate}
\end{defn}
\lec
