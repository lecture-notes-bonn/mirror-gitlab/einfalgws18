\begin{defn}
Let $\phi:\grpac{G}{X}$ be a group action of $G$ on $X$. Then
\begin{enumerate}
  \item $\phi$ is \emph{transitive}, if there is only one orbit in $G$.
  \item $\phi$ is \emph{faithful}, if the induced map $\phi':G\to S_X$ is injective.
  \index{group action!transitive}\index{group action!faithful}\index{transitive}\index{group action}.
\end{enumerate}
\end{defn}
\begin{lem}
  Let $\phi:\grpac{G}{X}$ be a group action. For any $x,y\in X$ the relation
  \[
  x\sim y :\Longleftrightarrow y\in G.x
  \]
  is an equivalence relation on $X$.
\end{lem}
\begin{proof}
\begin{itemize}
  \item $x\sim x$ holds, as $x\in G.x$ (by definition, $G.x \ni 1_G.x=x$ holds).
  \item For $x\sim y$, $y=g.x$ for some $g\in G$ holds. But then $x = g^{-1}.y$, so $x\in G.y$.
  \item Let $y = g.x$ and $z = h.y$ for some $g,h\in G$. Then $z = h.(g.x)=(hg).x$, so $z\in G.x$.
\end{itemize}
\end{proof}
\begin{cor}\label{9:orbitunion}
  Let $\phi:\grpac{G}{X}$ be a group action on $X$. Then $X$ is the disjoint union of the orbits of $G$.
\end{cor}
\begin{bsp}
\begin{enumerate}
  \item Let $H\leq G$ be a subgroup. Then
  \[
  \begin{tikzcd}[sep = small, cramped]
    H\times G\ar{r}&G\\
    (h,x)\ar[mapsto]{r}&hx
  \end{tikzcd}
  \qquad
  \text{and}
  \qquad
  \begin{tikzcd}[sep = small, cramped]
  H\times G\ar{r}&G\\
  (h,x)\ar[mapsto]{r}&xh^{-1}
  \end{tikzcd}
  \]
  both define a $H$-action on (the set) $G$. In this case, the orbits are the right/left cosets of $H$ in $G$. $\phi'$ is faithful, as $h.1_G = h1_G=h$ and $h^{-1}.1_G = h^{-1}$ for all $h\in H$. $\phi$ is transitive if and only if $G=H$.
  \item Let $H\leq G$ be a subgroup. Then
  \[
  \phi: H\times G\to G,~(h,x)\mapsto h.x:= hxh^{-1}
  \]
  defines a group action of $H$ on $G$. If $H=G$, then the orbits of $\phi$ are the conjugacy classes in $G$. The set of fixed points is given by
  \[
  G^H = \{ x\in G\ssp h.x = hxh^{-1}=h \text{ for all } h\in H\}.
  \]
  If $G=H$, then $G^H = Z(G)$.
  \item Let $G$ be a group and $X$ the set of all subgroups of $G$. Define
  \[
  \phi: G\times X \to X,~ (g,H)\mapsto g.H := gHg^{-1}.
  \]
  Then \[
  \{\text{orbits of }\phi \} = \{\text{conjugacy classes of subgroups in }G\}.
  \] For $H\in X$, the stabilizer of $H$ is
  \[
  G_H = \{ g\in G\ssp gHg^{-1}=H\} = N_G(H)
  \]
  exactly the normalizer of $H$ in $G$. The set of fixed points of $X$ is
  \[
  \{H\subset G\ssp g.H = gHg^{-1}=H \text{ for all}g\in G\} = \{H\leq G\ssp H\nsg G\}
  \]
  \end{enumerate}
\end{bsp}

\begin{lem}\label{9:gacard}
  Let $\phi:\grpac{G}{X}$ be a group action. Then
  \begin{enumerate}
  \item For all $x\in X$, the stabilizer $G_x$ is a subgroup of $G$.
  \item The map
  \[
  f: G/G_x \to G.x,~gG_x\mapsto g.x
  \]
  is a well-defined morphism of $G$-sets, where the $G$-action on $G/G_x$ is given by $g.(hG_x):=(gh)G_X$ and on $G.x$ given by $g.(h.x):= (gh).x$. In addition, $f$ is bijective.
  \item $\lvert G.x\rvert =[G:G_x]$, where we set $[G:G_x]=\infty$ if $\lvert G/G_x\rvert = \infty$.
\end{enumerate}
\end{lem}
\begin{proof}
\begin{enumerate}
\item For $x\in X$ and $g,h\in G_x$,
\[
(gh).x = g.(h.x) = g.x = x
\]
holds. In addition, for $g\in G_x$,
\[
g^{-1}.x = g^{-1}.(g.x) = (g^{-1}g).x  = 1.x = x.
\]
So $G_x\leq G$ is a subgroup.
\item Let $g_1,g_2\in G$ with $g_1G_x = g_2G_x$. Then $g_2^{-1}g_1\in G_x$, so
\[
x = (g_2^{-1}g_1).x \implies g_2.x = g_1.x
\]
and $f$ is well-defined.\
Surjectivity is clear, and injectivity follows from
\[
g_1.x= g_2.x \implies g_2^{-1}g_1.x = x \implies g_1G_x = g_2G_x.
\]
In addition, $f$ is a $G$-morphism, as
\begin{align*}
f(h.(gG_x)) = f((hg)G_x) &= (hg).x \\
&= h.(g.x)\\
&= h.f(gG_x).
\end{align*}
\item This follows, as $f$ is bijective.
\end{enumerate}
\end{proof}

\begin{nrem}
  The underlying idea of this lemma is the first isomorphism theorem for sets. Consider a set $M$ with an equivalence relation $\sim$ on $M$, and a map of sets $\pphi: M\to N$, such that $a'\sim a'' \Longleftrightarrow \pphi(a') = \pphi(a'')$. Then $\pphi$ factors uniquely as
  \[
  \begin{tikzcd}
  M\ar[\surarr]{r}\ar[bend left = 25]{rrr}{\pphi}&(M/\sim)\ar{r}{\cong}&\im \pphi \ar[\monarr]{r}&N
  \end{tikzcd}.
  \]\par
  In this case, we can consider the equivalence relation on $G$ $g_1\sim g_2 := \Longleftrightarrow g_1^{-1}g_2\in G_x$ (c.f. exercise sheet 4) and the surjective map $\begin{tikzcd}[sep = small, cramped]G\ar[\surarr]{r}&G.x\end{tikzcd},~g\mapsto g.x$.
\end{nrem}

\begin{thm}[\toidx{Orbit formular}]\label{9:orbitformular}
  Let $G$ be a group and $\phi:\grpac{G}{X}$ be a group action. Assume that $\abs{X}$ is finite, and let $G_{x_1},...,G_{x_n}$ represenetatives of the orbits $G$. Then
\[
  \abs{X} = \sum_{i=1}^n [G:G_{x_i}] = \abs{X^G}+\sum_{\subalign{1\leq & i \leq n\\ x_i& \notin X^G}}[G:G_{x_i}].
\]
\end{thm}
\begin{proof}
  By \cref{9:orbitunion}, $X$ is the disjoint union of the orbits under the $G$-action, so
  \[
  \abs{X} = \sum_{i=1}^n \abs{G.x_i}.\]
  Using \cref{9:gacard}, we get
  \[
  \abs{X} = \sum_{i=1}^n [G:G_{x_i}].\]
  In addition
  \begin{align*}
  x \in X^G &\iff G.x = \{x\}\\
  &\iff G_x = G \\
  &\iff [G:G_x] = 1
  \end{align*}
\end{proof}
\begin{thm}\label{9:grpordercentraliser}
  Let $G$ be a finite group, set $X:= G$ and consider the group action
  \[
  \phi: \grpac{G}{X},~(g,h)\mapsto ghg^{-1}.
  \]
  Then
  \[
  \abs{G} = \abs{Z(G)} + \sum_{\subalign{1\leq &i \leq n\\ x_i &\notin Z(G)}} [G:Z_G(x_i)] ,
  \]
  where the $x_1,\hdots,x_n$ are representatives of the different conjugacy classes of $G$, and $Z_G(x_i)$ denotes the centraliser of $x_i$.
\end{thm}
\begin{proof}
  This is just a special case of the orbit formular. For any $x\in X$, $G_x = Z_G(x)$ holds, and
  \begin{align*}
  x\in X^G & \iff gxg^{-1} = x \text{ for all }g\in G\\
  &\iff x\in Z(G)
  \end{align*}
\end{proof}

\section{Sylow's Theorems}
  In this section, $p$ denotes a prime number and groups are always finite, unless otherwise noted.
\begin{defn}
  A group $G$ is called a \toidx{$p$-group} if $\abs{G} = p^n$ for a $n\geq 0$. A subgroup $H\leq G$ is called a \emph{$p$-subgroup} if $H$ is a $p$-group (\coms Note that the trivial group is a $p$ group, for every prime number $p$\come ).
\end{defn}
\begin{thm}\label{9:pgrpab}
  Let $G \neq 1$ be a $p$-group. Then the center $Z(G)$ is non-trivial, and in particular, $G$ has a non-trivial abelian subgroup.
\end{thm}
\begin{proof}
Consider the conjugacy-action
\begin{equation}\label{9:pdivd}
\phi: \grpac{G}{G}~(g,h),\mapsto ghg^{-1}.
\end{equation}
By \cref{9:grpordercentraliser},
\[
\abs{G} = \abs{Z(G)} + \sum_{\subalign{1\leq &i\leq n\\x_i&\notin Z(G)}} [G:Z_G(x_i)].
\]
Now, by Lagranges Theorem
\[
p^n = \abs{G} = \abs{Z_G(x_i)}\cdot [G:Z_G(x_i)]
\]
holds. As $x\in Z(G)\iff Z_G(x_i) = G \iff \abs{Z_G(x_i)} = p^n$, for all $x\notin Z(G)$, $p \divd [G:Z_G(x_i)]$ follows. By \eqref{9:pdivd}, $p\divd \abs{Z(G)}$ follows, and hence the center is non-trivial.
\end{proof}
\begin{ncor}
  Let $G\neq 1$ be a $p$-Group. Then $p$ divides the order of the center of $
  G$.
\end{ncor}
\begin{ncor}
  Let $\abs{G} = p^2$ for some prime number $p$. Then $G$ is already abelian.
\end{ncor}
Before we prove this, we will collect some other nice facts about groups:
\begin{nprop}
  Let $G$ be a group.
  \begin{enumerate}
    \item $G$ can be written as the union of cyclic subgroups.
    \item $G$ is finite if and only if $G$ has only finitely many subgroups.
  \end{enumerate}
\end{nprop}
\begin{proof}
\begin{enumerate}
  \item As $g\in \gen{g}$ for all $g\in G$,
  \[
  G = \bigcup_{g\in G} g  \subseteq \bigcup_{g\in G}\gen{g}.
  \]
  \item Assume $G$ has only finitely many subgroups. Assume there is an element of inifinite order. But then $\gen{g}\cong \zz$. But $\zz$ has inifinitley many subgroups. So there is no element of infinite order. So each cylic group in $G$ has to be finite. As $G$ has only finitely many subgroups by assumption, each of which is finite, $G$ is finite by i).\par
  Assume that $G$ is finite. Then the power set of $G$ is finite, and so is the number of subgroups.
\end{enumerate}
\end{proof}
\begin{nprop}
Let $G$ be a group. Assume $G/Z(G)$ is cyclic. Then $G$ is abelian.
\end{nprop}
\begin{proof}
  Let $G/Z(G) = \gen{\hat{g}}$. Then for any $g,g'\in G$, there are $m,n\in \zz$ and $h,h'\in Z(G)$ such that $g = \hat{g}^mh$ and $g' = \hat{g}^nh'$. Then
  \begin{align*}
  gh &= \hat{g}^mh\hat{g}^nh'\\
  &= \hat{g}^m\hat{g}^nhh'\\
  &= \hat{g}^{m+n}hh'\\
  &= \hat{g}^{n+m}h'h\\
  &= \hat{g}^n\hat{g}^mh'h\\
  &= \hat{g}^nh'\hat{g}^mh = hg
  \end{align*}
\end{proof}
\begin{nprop}
Let $G$ be a group such that there is no subgroup $H$ with $Z(G)\subsetneq H \subsetneq G$. Then $G$ is abelian.
\end{nprop}
\begin{proof}
  By the corresponce of subgroups of $G$ containing $Z(G)$ and subgroups of $G/Z(G)$, $Z(G)$ has no non-trivial subgroup. By the above assertions, it is finite and in particular cyclic, which implies that $G$ is abelian.
\end{proof}
Now we can proceed and prove the original corollary:
\begin{proof}
  We can use Lagranges Theorem:
  \[
  p^2 = \abs{Z(G)}\cdot [G:Z(G)].
  \]
  By \cref{9:grpordercentraliser}, we know that $\abs{Z(G)}>1$.
  If $\abs{Z(G)} = p^2$, then $G = Z(G)$, so $G$ is abelian. If $\abs{Z(G)} = p$, then $[G:Z(G)]=p$, and hence $G/Z(G)$ is cyclic. But then the whole group is abelian by the above proposition (but this can never happen, as there is no prime number with $p=p^2$).
\end{proof}
\begin{lem}\label{9:grpordercongr}
  Let $G$ be a $p$-group and $\phi:\grpac{G}{X}$ a group action. Then
  \[
  \abs{X} \equiv \abs{X^G}\modu{p}
  \]
\end{lem}
\begin{proof}
This is analogous to the proof of \cref{9:grpordercentraliser}, but with different symbols: If $x_i \notin X^G$, then again $p\divd [G:G_{x_i}]$. Now the orbit formular (\cref{9:orbitformular}) implies that
\[
\abs{X} = \abs{X^G} + \sum_{\substack{1\leq i\leq n\\ x_i \notin X^G}} [G:G_{x_i}]
\]
so $p\divd \abs{Z^G}$.
\end{proof}

\begin{thm}[Cauchy\index{theorem! Cauchy's }]\label{9:cauchy}
  Let $G$ be a finite group with $p\divd \abs{G}$ for a prime number $p$. Then there is $g\in G$ such that $\ord(g) = p$.
\end{thm}
\begin{proof}
  Consider the set
  \[
  X:= \{ (g_1,\hdots,g_p)\in G^p\ssp g_1\cdot \hdots g_p = 1_G \} .
  \]
  Then $X \neq \emptyset$ as $1_G\cdot \hdots \cdot 1_G = 1_G$, and $g_p = (g_1\cdot \hdots g_{p-1})^{-1}$. So $\abs{X} = \abs{G}^{p-1}$. \\
  Consider now the group action
  \[
  \phi: \grpac{\zzq{p}}{X},~(n,(g_1,\hdots,g_p))\mapsto (g_{n+1},\hdots,g_p,g_1,\hdots,g_n).
  \]
  The set of fixed points of $\phi$ is given by
  \[
  X^{\zzq{p}} = \{ (g,\hdots,g)\in G^p \ssp g^p = 1\}.
  \]
  As $(1,\hdots,1)\in X^{\zzq{p}}$, $\abs{X^{\zzq{p}}}\geq 1$. Since $\zzq{p}$ is a $p$-group, by the above \cref{9:grpordercongr},
  \[
  \abs{X} \equiv \abs{X^{\zzq{p}}}\modu{p}.
  \]
  As $p\divd \abs{G}$, $p\divd \abs{X}$ follows and hence $\abs{X^{\zzq{p}}}\geq p$ follows. So there has to be a $1\neq g\in G$, such that
  \[
  (g,\hdots,g)\in X^{\zzq{p}}.
  \]
  As $p$ is prime, $\ord(g) = p$ follows.
\end{proof}
\begin{defn}
  Let $G$ be a group with $\abs{G} = p^r\cdot m$ for a prime number $p$ and $r\geq 0$ and $p\ndivd m$. We set
  \[
  \sylp(G):= \{ P \leq G \ssp \abs{P} = p^r\},
  \]
  the set of \emph{$p$-Sylow groups} of $G$ \index{sylow group}.
\end{defn}

\begin{rem}

\begin{enumerate}
\item Let $P\leq G$ be a $p$-Sylow group. For all $g\in G$, $gPg^{-1}$ is a $p$-Sylow group as well.
\item If there is only one $p$-Sylow group of $G$, then it is automatically a normal subgroup.
\end{enumerate}
\end{rem}

\begin{lem}\label{9:primnormal}
  Let $G$ be a finite group, and $U\leq G$ a $p$-subgroup of $G$. Then
  \[
  [N_G(U):U]\equiv [G:U]\modu{p},
\]
where
\[
N_G := \{ g\in G\ssp gHg^{-1}= H\}
\]
is the \toidx{normalizer} of $H$ in $G$.
\end{lem}

\lec
