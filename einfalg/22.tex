\begin{lem}\label{22: intermediate of Galois is Galois}
  Let $L/K$ be a Galois-extension, and $Z$ an intermediate field of $L/K$. Then $L/Z$ is a Galois-extension too.
\end{lem}
\begin{proof}
  By preceeding sections (\coms add this\come), $L/Z$ is finite, normal and separable, so Galois.
\end{proof}

\begin{lem}\label{22:algebraic implies galois equals all}
  Let $L$ be an algebraic field extension. Then
  \[
  \gal(L/K) = \fkcat (L,L).
  \]
\end{lem}
\begin{proof}
    Assume first that $L/K$ is finite. As every $K$-homomorphism $f\in \fkcat(L,L)$ is injective, it is surjective too, as $L$ is a finite-dimensional $K$-vector space and $f$ in particual $K$-linear.\par

    In the general case, consider any $a\in L$. As $L/K$ is algebraic, we can consider the minimal polynmoial $f:= \mip{a}{K} = \sum_{i=1}^n a_iX_i$. Then for every $\phi\in \fkcat(L,L)$, we have that $\phi(a)$ is a root of $f$, as
    \begin{align*}
    f\left(\phi(a)\right)&= \sum_{i=0}^n a_i \phi(a)^i\\
    &= \sum_{i=0}^n \phi(a_i)\phi(a)^i\\
    &= \sum_{i=0}^n \phi(a_ia^i)\\
    &= \phi\left(\sum_{i=0}^n a_i a^i\right)\\
    &= \phi\left(f(a)\right)\\
    &= \phi(0) = 0.
    \end{align*}
    Consider now the field extension
    \[
    L':= K\left(\lset \text{roots of }\mip{a}{K}\text{ in }L\rset \right).
    \]
    We have $\phi(L')\sse L'$, as $\phi$ only permutes the roots of $f$. This yields a well defined morphism $\restr{f}{L'}\in \fkcat(L',L')$, which is bijective (as $L'/K$ is finite).
    So in particual, $a\in \im \restr{\phi}{L'}\sse \im \phi$, and hence $\phi$ is surjective.
  \end{proof}
  \begin{ncor}
    Let $L/K$ be a field extension $f\in K[X]$ and $\phi\in \gal(L/K)$. Then for any root $a$ of $f$, $\phi(a)$ is too.
  \end{ncor}
  Let $L/K$ be a field extension, and $G\sse \aut(L)$ be a subgroup. Then the map
  \[
  G\times L \to L,~ (\phi,a)\mapsto \phi(a)
  \]
  is a group action of $G$ on $L$. As in previous chapters, we denote by
  \[
  L^G := \lset a\in L\ssp \phi(a)= a\text{ for all }\phi\in G\rset
  \]
  the set of fixed points of this action; for an element $a\in L$, we denote by
  \[
  G.a := \lset  \phi(a) \ssp \phi \in G\rset
  \]
  the orbit of $a$ under the $G$-action. The set $L^G$ is a subfield of $L$, as all $\phi \in \aut(L)$ are in particual ring homomorphisms.

\begin{lem}[Key Lemma]\label{22:keylemma}
  Let $L$ be a field, and $G\sse \aut(L)$. Let $a\in L$. If $G.a\sse L$ is finite, then
  \begin{enumerate}
    \item a is separable over $L^G$.
    \item the minimal polynomial $\mip{a}{L^G}$ has the special form
    \[
    \mip{a}{L^G} = \prod_{b\in G.a}\left(X-b\right) \in L^G[X].
    \]
    \item $\deg (\mip{a}{L^G}) = \left[L^G(a):L^G\right] = \abs{G}$.
  \end{enumerate}
\end{lem}
\begin{proof}
Consider the polynomial
\[
f:= \prod_{b\in G.a}\left(X-b\right),
\]
which is well-defined, as $G.a$ is finite. For all $\phi\in G$, the restriction to the orbit $\restr{\phi}{G.a}$ is bijectiv, as
\[
\phi\circ \psi(a)=\phi\circ \psi'(a)\implies \psi(a)=\psi'(a)
\]
for all $\psi,\psi'\in \aut(L)$. So
\begin{align*}
  \phi(f) &= \prod_{b\in G.a} \left(X-\phi(b)\right)\\
          &= \prod_{b\in G.a} \left(X-b\right) = f.
\end{align*}
  This implies $f\in L^G[X]$. As $f(a)=0$, we have that $a$ is an algebraic element of $L/L^G$, and hence $\mip{a}{L^G}\divd f$. \par
  We also have $G\sse \gal(L/L^G)$\footnote{But not necessarily $G = \gal(L/L^G)$ - there can be more morphisms $L\to L$ that fix all elements that are fixed by $G$, but not in $G$. More on that later.}. So $\phi(a)$ is a root of $\mip{a}{L^G}$, for any $\phi \in G$, i.e.
  \[
  \begin{tikzcd}[sep = small, cramped]
  \lset \phi(a)\ssp \phi \in G\rset \ar[equal]{d}&
  \sse &
  \lset \text{roots of }\mip{a}{L^G}\rset\\
  G.a&
  &
  \end{tikzcd},
  \]
  and $\abs{G.a} = \deg f$, by construction of $f$. Hence $\deg f \leq \deg \mip{a}{L^G}$. As $f$ is monic, $f=\mip{a}{L^G}$ follows, which shows ii). Claim iii) follows immediately, and i) by defintion of separability.
\end{proof}
\begin{cor}\label{22:algebraic from automorphisms implies sep and normal}
  Let $L/K$ be an algebraic field extension, and $G\sse \gal(L/K)$ a subgroup. Then $L/L^G$ is separable and normal.
\end{cor}
\begin{proof}\leavevmode
\begin{itemize}
  \item Let $a\in L$ and $f:= \mip{a}{K}\in K[X]$ the minimal polynomial of $a$. Then $f(\phi(a))=0$ for all $\phi\in G$. As $f$ has only finitely many roots, the orbit $G.a$ is finite too. By the \hyperref[22:keylemma]{key lemma}, we have
  \[
  \mip{a}{L^G} = \prod_{b\in G.a} \left(X-b\right),
  \]
  hence $L/L^G$ is separable.
  \item Let $f\in L^G[X]$ be irreducible. Assume $f$ has a root $a\in L$. Then $f = e \mip{a}{L^G}$, where $e$ is a unit in $\unts{L^G}$. Then by the second part of the \hyperref[22:keylemma]{key lemma}, $f$ splits over $L$ in linear factors. So $L/L^G$ is normal.
  \end{itemize}
\end{proof}
\begin{thm}\label{22: finite group if finite extension galois}
  Let $L$ be a field, and $G\sse \aut(L)$ a subgroup. Then the following are equivalent:
  \begin{enumerate}
    \item $G$ is a finite group.
    \item $L/L^G$ is a finite field extension.
  \end{enumerate}
\end{thm}
\begin{proof}
  Let $K:= L^G$. Then $G\sse \gal(L/L^G)$.\\
    i) $\implies$ i): Assume $L/K$ is finite. Then $L/K$ is in particular algebraic. By \cref{22:algebraic implies galois equals all}, we have $\gal(L/K)=\fkcat(L,L)$. By \cref{18: injective lifting}, the inclusion map $\gal(L/K)\membarr \fkcat(L,\overline{L})$
    is injective. So we have
    \[
    \abs{G}\leq \abs{\gal(L/K)}\leq \abs{\fkcat(L,\overline{L})},
    \]
    where the last inequality was shown in \cref{20: upper bound for galois group}. \par
    ii) $\implies$ i): Let $Z$ be an intermediate field of $L/K$. As $L/K$ is separable by the \hyperref[22:keylemma]{key lemma}, $Z/K$ is too (by \cref{20:sep1}). As $Z/K$ is finite, by the \hyperref[21: finite and sep has primitive]{primitive element theorem}, there is a primitive element $a\in Z$, i.e. $Z=K(a)$. Then $\deg \mip{a}{L^G}=[Z:K]$. Since we can regard $a$ as an element in $L$, we can apply the \hyperref[22:keylemma]{key lemma}, to get
    \begin{equation}\label{22:degree bound}\tag{$\ast$}
    [Z:K]=\deg \mip{a}{L^G}= \abs{G.a}\leq \abs{G}.
    \end{equation}
    Assume now that $[L:K]>\abs{G}$. Then there is finite linear independent subset $A\sse L$ with $\abs{A}>\abs{G}$. But for the finite field extension $Z:= K(A)$, $[Z:K]\geq \abs{A}>G$ would hold, contradicting \eqref{22:degree bound}.  Hence $[L:K]\leq \abs{G}<\infty$.\par
    Assume now that $G$ or $L/K$ is finite. By i) $\implies$ ii), we have $\abs{G}\leq [L:K]$, and by ii) $\implies$ i), we have $[L:K]\leq \abs{G}$, hence $[L:K]= \abs{G}$. As $G\sse \gal(L/K)$, we also have $\abs{G}\leq \abs{\gal(L/K)}$. By i) $\implies$ ii), we alos have $\abs{\gal(L/K)}\leq [L:K] = G$, so $\abs{G} = \abs{\gal(L/K)}$, and hence $G = \gal{L/K}$.
\end{proof}
\begin{nrem}
  In this case, the field extension $L/L^G$ is a Galois-extension, which follows from \cref{22:algebraic from automorphisms implies sep and normal}, and $G = \gal(L/K)$
\end{nrem}
%fix numbering on this one too, find better names for $\Psi$ and $\Phi$.
\begin{cor}\label{22: Galois correspondence for finite fields}
  Let $L/K$ be a finite field extension, and $G:= \gal(L/K)$. Consider the maps:
  \[
  \begin{tikzcd}
    \lset H \ssp H\leq G\rset \ar[shift left]{r}[above]{\Phi} &
    \lset Z\text{ subfield of }L/K\rset \ar[shift left]{l}[below]{\Psi}\\
    H\ar[mapsto]{r}&
    L^H\\
    \gal(L/Z)\ar[mapsfrom]{r}&
    Z
  \end{tikzcd}
  \]
  Then $\Psi\circ \Phi = \id$; in particular, $\Phi$ is injective and $\Psi$ is surjective.
\end{cor}
\begin{proof}
  We have $G =  \gal(L/L^G)$.
\end{proof}


\begin{thm}\label{22: equivalent characterisation of Galois extensions}
  Let $L/K$ be a finite field-extension. Then the following are equivalent.
  \begin{enumerate}
    \item $L/K$ is a Galois-extension.
    \item $[L:K] = \abs{\gal(L/K)}$.
    \item $K = L^{\gal(L/K)}$.
    \item For all $a\in L$
    \[
    \mip{a}{K} = \prod_{\substack{b\in \\\gal(L/K).a}}\left(X-b\right)
    \]
    holds.
    \item $L$ is a splitting field of a separable $f\in K[X]$.
  \end{enumerate}
\end{thm}
\begin{proof}
Let $G := \gal(L/K)$. Then $\abs{G}\leq [L:K]\leq \infty $, by \cref{20: upper bound for galois group}.\par
  i) $\implies$ ii): Let $L/K$ be a Galois-extension. Then $L/K$ is in particular normal and separable. By normality and \cref{19: normal characterisations}, we have $\fkcat(L,L)\cong \fkcat(L,\overline{L})$,
  and by \cref{22:algebraic implies galois equals all} $G=\fkcat(L,L)$ (as finite extensions are in particular algebraic).
  So $\abs{G} = \abs{\fkcat(L,\overline{L})}$. By separability and \cref{20: number of morphisms}, we have $\fkcat(L,\overline{L}) = [L:K]$ (where we again use the identification $\overline{K}\cong \overline{L}$).\par
  ii) $\implies$ iii): Assume $[L:K] = \abs{\gal(L/K)}$. $L^G$ is an intermediate field of $L/K$. By \cref{20: upper bound for galois group}, we have $[L:L^G]\geq \abs{L/L^G}$.
  By \cref{22: finite group if finite extension galois}, we have $G = \gal(L/L^G)$, and hence $\abs{G}=\abs{\gal(L/L^G)} = [L:K]$. As $K\sse L^G$, we also have $[L:K]\geq [L:L^G]$. So in total
  \[
  [L:L^G]\geq \abs{\gal(L/L^G)} = \abs{G} = [L:K] \geq [L:L^G],
  \]
   and hence $[L:K]=[L:L^G]$, which implies $K = L^G$. \par
  iii) $\implies$ iv): This is part ii) of the \hyperref[22:keylemma]{key lemma}.\par
  iv) $\implies$ v): By iv), every $a\in L$ is separable over $K$. Hence $L/K$ is a separable extension. By the \hyperref[21: finite and sep has primitive]{primitive element theorem}, there is a $b\in K$ such that $L = K(b)$. In this case, $L$ is a splitting field for $\mip{b}{K}$.\par
  v) $\implies$ i): Let $L$ be a splitting field of a separable $f\in K[X]$. Then by \cref{19: normal iff splitting for finite}, $L/K$ is normal. For every root $a$ of $f$ in $L$, $\mip{a}{K}\divd f$. As $f$ is separable, this implies that $a$ is separable over $K$.
  As $L = K\left(\lset \text{roots of }f\text{ in }\overline{K}\rset\right)$, by \cref{20:stepwisesep} $L/K$ is separable. Hence $L/K$ is a Galois-extension.
\end{proof}
\begin{thm}\label{22: galois hull}
Let $L/K$ be a finite and separable field extension, and $N$ the normal hull of $L/K$. Then
\begin{enumerate}
  \item $N/K$ is a Galois-extension.
  \item $N/K$ is the smallest extension with this property; i.e. for any intermediate field $Z$ of $N/K$ such that $Z/K$ is a Galois-extension $Z=N$ holds.
  \item $N$ is universal with this property, i.e. for a field extension $N'/L$ satisfying properties i) and ii), there is a $L$-isomorphism $N'\to N$.
\end{enumerate}
\end{thm}
\begin{proof}
  \coms Ommited.
\end{proof}
\begin{defn}
In this case, the extension $N/K$ is the \toidx{Galois-hull} of $L/K$.
\end{defn}
\lec
