\begin{thm}
  Let $L/K$ be a finite field extension. Then the following are equivalent:
  \begin{enumerate}
    \item There exists a primitive element for $L/K$.
    \item The set $\lset Z \text{ intermediate field of }L/K\rset$ is finite.
  \end{enumerate}
\end{thm}
\begin{proof}
  \coms In the script, is supposed to be analogous to the proof of the existence of primitive elements. \come
\end{proof}
\section{Fundamental Theorem of Galois Theory}
We now prove the fundamtental theorem of Galois theory.
\begin{thm}\index{Galois! fundamental theorem of}\label{23: fundamental theorem galois}
Let $L/K$ be a Galois-extension, $G:= \gal(L/K)$, and $Z$ an intermediate field of $L/K$.
Then
\begin{enumerate}
  \item The assignment
    \[
    \begin{tikzcd}
      \lset H \ssp H\leq G\rset \ar[shift left]{r}[above]{\Phi} &
      \lset Z\text{ subfield of }L/K\rset \ar[shift left]{l}[below]{\Psi}\\
      H\ar[mapsto]{r}&
      L^H\\
      \gal(L/Z)\ar[mapsfrom]{r}&
      Z
    \end{tikzcd}
    \]
    is bijective, i.e. for any subgroup $H\leq G$ we have $H = \gal(L/L^H)$ and for any indermediate field $K\sse Z \sse L$ we have $Z = L^{\gal(L/Z)}$.
    \item We have $\abs{\gal(L/Z)} = [L:Z]$ and for any subgroup $H\sse G$, $H\leq G$, $\abs{H} = [L:L^H]$ holds.
    \item Let  $\phi \in \gal(L/K)$. Then
    \[
    \gal\left(L / \phi(Z)\right) = \phi \gal\left(L/Z\right)\phi^{-1}
    \]
    holds.
    \item The following are equivalent
    \begin{enumerate}[label = \alph*)]
      \item $Z/K$ is a Galois-extension.
      \item $\gal(L/Z)$ is a normal subgroup of $G$.
    \end{enumerate}
    In this case, there is a well-defined group isomorphism
    \[
    G/\gal(L/Z) \to \gal(Z/K),~ \phi\cdot \gal(L/Z)\mapsto \restr{\phi}{Z}.
    \]
    \item Let $H,H'\sse G$ be subgroups. Then under $\Phi$, we have:
    \[
    \begin{tikzcd}[sep = small]
    \langle H,H'\rangle \ar[mapsto]{r}[above]{\Phi}& L^H\cap L^{H'}
    \end{tikzcd}
    \qquad \text{and}\qquad
    \begin{tikzcd}[sep = small]
      H\cap H'\ar[mapsto]{r}[above]{\Phi}&
      L^H.L^{H'}
    \end{tikzcd}
    \]
    Let $Z'$ be another indermediate field of $L/K$. Then under $\Psi$, we have:
    \[
    \begin{tikzcd}[sep = small]
      Z.Z'\ar[mapsto]{r}[above]{\Psi}&
      \gal(L/Z)\cap \gal(L/Z')
    \end{tikzcd}
    \qquad \text{and}\qquad
    \begin{tikzcd}[sep = small]
      Z\cap Z'\ar[mapsto]{r}[above]{\Psi}&
      \langle \gal(L/Z),\gal(L/Z')\rangle
    \end{tikzcd}
    \]
  \end{enumerate}
\end{thm}


\begin{proof}
  The extension $L/Z$ is again a Galois extension, by \cref{22: intermediate of Galois is Galois}.
\begin{enumerate}
  \item We have  $Z = L^{\gal(L/Z)}$
    by \cref{22: equivalent characterisation of Galois extensions}). So $\Phi\circ \Psi = \id$.
  By \cref{22: Galois correspondence for finite fields}, we also have $\Psi \circ \Phi = \id$.
  \item Again by \cref{22: equivalent characterisation of Galois extensions}, we have $\abs{\gal(L/Z)} = [L:Z]$, and $\abs{H} = [L:L^H]$ for any subgroup $H \sse G$
  (by \cref{22: Galois correspondence for finite fields}).
  \item Let $Z$ be an intermediate field of $L/K$ and $\phi \in G$. Then $\phi(Z)$ is an intermediate field of $L/K$, as $\phi$ is an automorphisms. We also have $\phi \gal(L/Z)\phi^{-1}\sse G$, as $\gal(L/Z)\sse G$. By considering $\phi$ as a map $Z \to \phi(Z)$, we get $\phi \gal(L/Z)\phi^{-1}\sse \gal(L/\phi(Z))$.
  As $\phi$ is an automorphism, we also have $[L:\phi(Z)] = [L:Z]$, $[\phi(Z):K] = [Z:K]$ and $\phi: Z\to \phi(Z)$ is an isomorphism. Hence
  \[
  \abs{\phi \gal(L/Z)\phi^{-1}} = \abs{\gal(L/Z)} = [L:Z] = [L:\phi(Z)] = \abs{\gal\left(L/\phi(Z)\right)},
  \]
  where we used \cref{22: equivalent characterisation of Galois extensions} ii) for to get the equality of the degree of the extension and the cardinality of the Galois group.
  \item By iii), we have
  \begin{equation}\label{22: normal condition}\tag{$\ast$}
  \phi \gal(L/Z)\phi^{-1} = \gal(L/\phi(Z)) \text{ for all }\phi \in G.
  \end{equation}
  \begin{itemize}
    \item a) $\implies$ b): Assume $Z/K$ is a Galois extension, so in particular normal. Then we have by \cref{19: normal characterisations} iii) that $\phi(Z) = Z$ for all $\phi \in G$, as we can regard every $K$-homomorphism $\phi \in G$ by restriction as a $K$-homomorphism $\phi \in \gal(Z/K)$.
    So by \eqref{22: normal condition}, we have $\gal(L/Z)\nsg G$.
    \item b) $\implies$ a): Assume $\gal(L/Z)\nsg G$. Let $f\in \fkcat(Z,\overline{L})$ (we will later use the identification $\overline{Z} \cong \overline{L}$). By \hyperref[18:artin]{Artin's Theorem}, we can lift $f$ to a morphism $\phi \in \fkcat(L,\overline{L})$, as $L/Z$ is finite. Furthermore, as $L/K$ is normal, we have $\fkcat(L,\overline{L})\cong \gal(L/K)$
    (by normality of the extension, we get $\fkcat(L,\overline{L})\cong \fkcat(L,L) = \gal(L/K)$).
    Now the normality of $\gal(L/Z)\nsg \gal(L/K)$ implies
    \[
      \gal(L/Z) = \phi \gal(L/Z)\phi^{-1} = \gal(L/\phi(z)).
    \]
    As $\psi$ is injective, we get $Z = \phi(Z)$. As $\restr{\phi}{Z} = f$ by construction, we have $f(Z) = Z$. So $Z/K$ is nornmal (by \cref{19: normal characterisations}). \par
    In this case, we get a well-defined group homomorphism
    \[
    \restrop_Z : G\to \gal(Z/K),~\phi\mapsto \restr{\phi}{Z}.
    \]
    The kernel of $\restrop$ is given by
    \begin{align*}
      \ker \left(\restrop_Z\right) &=
      \lset \phi: L \to L \;\middle|\;
      \begin{tabular}{@{}l@{}}
        $\phi$ is a $K$-automorphism,\\
        $\restr{\phi}{Z} = \id_Z$.
       \end{tabular}
       \rset\\
       &= \gal(Z/K).
    \end{align*}
    In addition, $\restrop_Z$ is surjective: For every $f\in \gal(Z/K)$, there is a lift $\phi'\in \fkcat(L,\overline{L})$, as $L/Z$ is finite. As $L/K$ is a normal extension, we have $\phi'(L) = L$, so we can regard $\phi'$ as an element of $\gal(L/K)$. By construction, we have $\restr{\phi''}{Z} = f$.\par
    The claim follows now from the first isomorphism theorem for groups. %todo: add reference.
  \end{itemize}
    \item This follows from \cref{22: phi and psi are contravariant}.

\end{enumerate}
\end{proof}
\begin{cor}
  Let $L/K$ be a finite and separable field extension. Then there are only finitely many intermediate fields of $L/K$.
\end{cor}
\begin{proof}
  Let $N$ be the Galois hull of $L/K$. Then by \cref{22: galois hull}, $N/K$ is a Galois extension, so by the \hyperref[23: fundamental theorem galois]{fundamental theorem of Galois theory}, there are only finitely many intermediate fields of $N/K$. As every intermediate field of $L/K$ is in particular one for $L/K$, the claim follows.
\end{proof}
\begin{defn}
  Let $L/K$ be a Galois-extension. We say $L/K$ is \emph{cyclic}\index{Galois extension! cyclic}/\emph{abelian}\index{Galois extension! abelian}/\emph{solvable}\index{Galois extension! solvable}, if $\gal(L/K)$ is cyclic/abelian/solvable.
\end{defn}
\begin{ncor}
  Let $L/K$ be an abelian Galois-extension. Then for every intermediate field $Z$ of $L/K$, the extension $Z/K$ is again a Galois extension.
\end{ncor}
\begin{proof}
  As $\gal(L/K)$ is abelian, every subgroup is normal, so in particular $\gal(L/Z)$ for an intermediate field $Z$. Then claim follows from \cref{23: fundamental theorem galois} iv).
\end{proof}
%this was originally one in the lecture
\begin{remdef}
  Let $L/K$ be a Galois extension, and $Z$ an intermediate field of $L/K$. Consider the set
  \[
  M = \lset \phi(Z) \ssp \phi \in \gal(L/K) \rset.\]
  The elements of $M$ are called the \emph{conjugates} of $Z$ \index{conjugate! of intermediate field}. $M$ is finite, by \cref{22: equivalent characterisation of Galois extensions} ii).
\end{remdef}
\begin{lem}
  Let $Z$ be an intermediate field of a Galois extension $L/K$, and $M$ the set of the conjugates of $Z$, $M = \lset Z_1,\hdots,Z_k\rset$. Then the Galois hull of $Z/K$ is given by
  $M := Z_1.\hdots .Z_k$.
\end{lem}
\begin{proof}
  $M/K$ is finite and separable (\coms reference to compositum of finite fields is missing\come) (\cref{20:compsep}).
  \begin{itemize}
    \item $M/K$ is normal: We can identify $\overline{M} = \overline{L}$. Every $K$-homomorphism $f:M\to \overline{L}$ can be lifted to a $K$-homomorphism $f':L\to \overline{L}$. As $L/K$ is normal, we have $f'(L) = L$ (\cref{19: normal characterisations}), so $f'\in \gal(L/K)$.
    Hence $f = \restr{f'}{M}$. By the definition of $M$, we have $\phi(M) = M$ for all $\phi\in \gal(L/K)$, so the claim follows.
    \item Minimality of $M$: Let $M'/K$ be normal with $Z\sse M' \sse L$. Then $\phi(M') = M$ for all $\phi\in \gal(L/K)$. So $\phi(Z)\sse M'$, and hence $M\sse M'$.
  \end{itemize}
\end{proof}
\section{Examples for the Fundamental Theorem of Galois Theory}
\coms \textit{A lot of examples} are still missing.\come

\section{Inverse Problem of Galois Theory}
\begin{probl}
  Let $K$ be a field, and $G$ a finite group. Is there a field extension $L/K$ such that $\gal(L/K) \cong G$?
\end{probl}
There are several answers:
\begin{itemize}
  \item If $K$ is finite, this is in general not the case.
  \item This is always possible for $K\cc(X)$.
  \item This is an open problem for $K=\qq$. It is a theorem of Weber and Kronecker that for an \textit{abelian} group $G$, there is a Galois extension $L/K$, such that $\gal(L/\qq)\cong G$.
\end{itemize}
The problem becomes easier if we don't require $K$ to be fixed.
\begin{prop}
  Let $G$ be a finite group. Then there is a Galois-extension $L/K$ such that $\gal(L/K) \cong G$.
\end{prop}
\begin{proof}
  Let $K$ be a field, $n\geq 1$ and $L:= K(X_1,\hdots,X_n)$. Then for every $\sigma \in S_n$ can be identified with a $\sigma \in \aut(L)$ which permutes the variables. So we can regard $S_n$ as a subgroup of $\aut(L)$. So for any subgroup $H\leq S_n$, we can consider the Galois extension $L/L^H$, where $H\cong \gal(L/L^H)$ holds (\cref{22: finite group if finite extension galois}).\par
  By Cayleighs Theorem, we can regard every finite group as a subgroup of $S_n$ for a sufficiently large $n$ (e.g. $n = \abs{G}$). So the claim follows.
\end{proof}

\lec
