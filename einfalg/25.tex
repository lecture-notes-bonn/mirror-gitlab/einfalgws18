\begin{reminder}\label{25: recall construction properties}
  \begin{enumerate}
  \item Let $M\sse \cc$ be a subset with $0,1\in M$. Let $K_0 := \qq(M\cap \overline{M})$, and consider the field extension $M_{\infty}/K_0$ of \emph{contructible numbers} from $M$. Then $M_{\infty}/K_0$ is an algebraic field extension.
  \item Let $z\in \cc$. Then the following are equivalent:
   \begin{enumerate}[label = \alph*)]
     \item $z\in M_{\infty}$.
     \item There is an intermediate field $L$ of $\cc/K_0$ such that $z\in L$, and $L$ can be constructed from $K_0$ by successively adjoining square roots.
     \item There is a chain
     \[
     K_0 = L_0 \sse \hdots \sse L_n \sse \cc
     \]
     of subfields, with $[L_i:L_{i-1}] = 2$, and $[L_i:L_{i-1}] = 2$ for all $1\leq i \leq n$, such that $z \in L_n$.
   \end{enumerate}
   In this case, $[K_0(z):K_0] = 2^m$ for a $0\leq m \leq n$.
   \end{enumerate}
\end{reminder}
\begin{thm}\label{25: constructible iff 2metacyclic}
  Let $M$ be as above, and $z\in \cc$. Then the following are equivalent:
  \begin{enumerate}
    \item $z\in M_{\infty}$.
    \item There is an intermediate field L of $\cc/K_0$ such that $z\in L$ and $L/K_0$ is $2$-metacyclic.
    \item $\gal(\mip{z}{K_0})$ is a $2$-group.
  \end{enumerate}
\end{thm}
\begin{proof}
  i) $\iff$ ii): Let $L$ be an intermediate field of $\cc/K_0$. Then $L/K_0$ is $2$-metacyclic if and only if it can be constructed from $K_0$ by successively adjoining square roots. So the claim follows from \cref{25: recall construction properties}. \par
  ii) $\implies$ iii): Let $L$ be as in ii).
  \begin{itemize}
    \item Without loss of generality, we can consider the case $L = K_0(z)$: By assumption, there is an ascending chain of the form
    \[
    K = L_0 \sse \hdots \sse L_t = L
    \]
    such that $z \in L$ and $[L_i:L_{i-1}]= 2$. Set $L_i' := K_0(z)\cap L_i$. Then we get another ascending chain
    \[
    K_0' = L_0' \sse L_1' \sse \hdots \sse L_t'
    \]
    such that $[L_i':L_{i-1}']\leq 2$.
    \item Let $Z$ be a splitting field of $\mip{z}{K_0}$. We can then regard the extension $Z/K_0$ as the Galois hull oof $L/K_0$. By \cref{24: galois hull metacyclic}, we have that $Z/K_0$ is again $2$-metacyclic.
  \end{itemize}\par
  iii) $\iff$ iv): Let $Z$ be a splitting field of $\mip{z}{K_0}$. Then $Z/K_0$ is a Galois extension. By the \hyperref[23: fundamental theorem galois]{fundamental theorem of Galois theory}, we have that $[Z:K_0]= 2^m$ if and only if  $\gal(Z/K_0)$ is a $2$-group.\par
  Let $L$ be the splitting field of $\mip{z}{K_0}$. Then $L/K_0$ is a Galois extension such that $[L:K_0]=2^m$. By \cref{24: metacyclic iff solvable}, this implies that $L/K_0$ is $2$-metacyclic. So by \cref{25: constructible iff 2metacyclic},
  we have thaat $z\in M_{\infty}$.
\end{proof}

\section{Cyclotomic Polynomials}
Let $K$ be a field.
\begin{defn}
\leavevmode
\begin{enumerate}
\item
  Let $K$ be a field, and $n\geq 1$.
  For
  \[
  f = X^n-1 \in K[X]
  \]
  we consider the set
  \[
  \zeron:= \zeron(K):= \lset x\in \overline{K} \ssp f(x) = 0 \rset
  \]
  of roots of $f$ in $\overline{K}$. The Elements of $\zeron(K)$ are called the $n$-th \toidx{roots of unity.}
  \item The field extension $L:= K\left( \zeron(K)\right)$ is called the $n$-th \toidx{cyclotomic field}.
\end{enumerate}
\end{defn}
\begin{rem}
  Let $\rchar K = p > 0$. Let $n = mp^s$, with $s\geq 0, m\geq 1$ and $p\ndivd m$. Then
  \[
  f = X^n -1 = X^{m^{(p^s)}} -1 = \left(X-1\right)^{(p^s)},
  \]
  so $K\left(\zeron(K)\right) = K\left(\mathbb{E}_m(K)\right)$. So we will assume
  \begin{enumerate}
    \item $\rchar(K) =0$, or
    \item $\rchar K = p >0$ and $p\ndivd n$.
  \end{enumerate}
\end{rem}
\begin{nlem}
  Let $f\in K[X]$ be a polynomial. Then $f$ is inseparable if and only if $f$ and $f'$ are relativley prime.
\end{nlem}
\begin{lem}
  We have $\abs{\zeron(K)} = n$ and $f$ is separable over $K$.
\end{lem}
\begin{proof}
  We have $f' = nX^{n-1} \neq 0$, by the above assumption. So $f'$ has no roots different from zero. So (by the above lemma) $f$ is separable, and hence $f$ has $n$ different roots in $\overline{K}$.
\end{proof}
\begin{ncor}
  Let $L:= K\left( \zeron(K)\right)$. Then $L/K$ is a Galois extension.
\end{ncor}
\begin{proof}
    $L$ is a splitting field of the separable polynomial $f = X^n-1 \in K[X]$.
\end{proof}
\begin{lem}
  $\zeron(K)$ is a cyclic subgroup of $\unts{\overline{K}}$.
\end{lem}
\begin{proof}
\begin{itemize}
  \item As $0\notin \zeron(K)$, we have $\zeron(K)\sse \unts{\overline{K}}$.
  \item We have $1\in \zeron(K)$, and for $a,b\in \zeronk$, we have
  \[
  (ab)^n -1 a^nb^n-1 = 0\qquad \text{and}\qquad \left(a^{-1}\right)^n -1 = \left(a^n\right)^{-1}-1 =0.
  \]
\end{itemize}
As $\zeronk$ is finite, the claim follows from \cref{8:cycunits}.
\end{proof}
\begin{cor}
  For every $n\geq 1$, we have $\zeronk \cong \zz_n$.
\end{cor}
\begin{reminder}[\cref{8: generators of znz}]
  For $n\geq 1$, the relation
  \[
  \pphi(n) = \lvert \{ \text{generators of }\zz/n\zz\}\rvert = \lvert(\zz/n\zz)^{\times}\rvert,
  \]
  holds.
\end{reminder}
\begin{defn}
  The set of $\zeronk$ are the \emph{primitive $n$-th roots of unity} \index{roots of unity! primitve}.
\end{defn}
\begin{bsp}
  \coms The scetch of the primitive elements of $\mathbb{E}_{12}(\cc)$ is ommited.\come
\end{bsp}
\begin{lem}
  Let $z \in \zeronk$ be a primitive $n$-th root of unity. Then $K\left(\zeronk\right)=K(z)$.
\end{lem}
\begin{proof}
\coms This is should be obvious.\come
\end{proof}\label{25: order of galois group of primitive extension divides phi(n)}
\begin{prop}
  Let $L:= K\left(\zeronk\right)$. Then $\gal(L/K)$ is abelian, and
  \[
  \abs{\gal(L/K)} \divd \pphi(n).
  \]
\end{prop}
\begin{proof}
  Let $G:= \gal(L/K)$ and $\phi \in G$.
  \begin{itemize}
    \item Let $z \in \zeronk$ be primitive. We have
    \begin{align*}
      \left(\phi\left(z^i\right)\right)^n &= \left(\phi\left(z^n\right)\right)^i\\
      &= \left(\phi\left(1\right)\right)^i\\
      &= 1,
    \end{align*}
    for all $0\leq i\leq n$. So $\phi(\zeronk)\sse \zeronk$, and we get a well-defined restriction
    \[
    \restr{\phi}{\zeronk}:\zeronk \to \zeronk,\]
    which is an injective group homomorphism. As $\zeronk$ is finite, we have $\restr{\phi}{\zeronk}\in \aut(\zeronk)$.
    \item In this way, we become a well-defined restriction map
    \[
    \eta: G\to \aut(\zeronk),~\phi \mapsto \restr{\phi}{\zeronk},
    \]
    which is also a group homomorphism.
    As $L/K$ is finite, every $\phi\in G$ is already uniquely determined by $\restr{\phi}{\zeronk}$ (by Artin). So $\eta$ is injective, and $G \cong \im \eta \sse \aut(\zeronk)$. As $\zeronk$ is cyclic, this implies that
    \[
    \abs{G} = \abs{\im \eta} \divd \abs{\aut(\zeronk)} = \phi(n).
    \]
  \end{itemize}
\end{proof}
\begin{bsp}
  \leavevmode
  \begin{enumerate}
    \item Let $K = \ff_2$, and consider the polynomial $f:= X^3-1$. Then $f$ factors into irreducibles as
    \[
    f = \left(X-1\right)\left(X^2+X+1\right).
    \]
    Every root $a\neq 1$ of $f$ in $\overline{\ff_2}$ is primitive third root of unity, and has minimal polynomial $X^2+X+1$. In particular, we have $[\ff_2(a):\ff_2]=2$.
    \item Let $K = \ff_2$, and consider the polynomial $g:= X^7-1$. Then $g$ factors into irreducibles as
    \[
    g = \left(X-1\right)\left(X^3+X^2+1\right)\left(X^3+X+1\right).
    \]
    There are now 6 primitive roots of unity, but they can have different minimal polynomials.
  \end{enumerate}
\end{bsp}
\begin{goal}
We want to show later: For $K=\qq$, every primitive $n$-th root of unity has the same minimal polynomial, of degree $\phi(n)$.
\end{goal}
\begin{lem}
  Let $z$ be a primitive $n$-th root of unity. Then the following are equivalent:
  \begin{enumerate}
    \item $z^m$ is primitive.
    \item $\gcd(m,n) = 1$.
  \end{enumerate}
\end{lem}
\begin{proof}
  This is \cref{8: generators of znz}.
\end{proof}
\lec
