\begin{proof}
  Let $X := G/U$ and consider the group action of $U$ on the left-cosets,
  \[
   \phi: \grpac{U}{X},~(u,gU)\mapsto (ug)U.
   \]
   This is well-defined, as $g_1U=g_2 U \implies g_1^{-1}g_2 \in U\implies g_1^{-1}u^{-1}ug_2\in U \implies (ug_1)U = (ug_2)U$). The set of fixed points is given by
   \begin{align*}
   X^U &=\{ gU \in X\ssp ugU = gU \text{ for all }u\in U\}\\
   &= \{ gU \in X \ssp g^{-1}ug \in U \text{ for all }u\in U\}\\
   &= \{ gU \in X\ssp g\in N_G(U)\} = N_G(U)/U,
   \end{align*}
   so $\abs{X^U} = [N_G(U):U]$.\\
   In addition, $\abs{X} = [G:U]$ holds. So by \cref{9:grpordercongr}, we get
   \[
   [N_G(U):U] = \abs{X^U} \equiv \abs{X} = [G:U]\modu{p}.
   \]
\end{proof}
\begin{cor}\label{10:nonormal}
  Let $G$ be a finite group, and $U\leq G$ a $p$-subgroup. With $p\divd [G:U]$. Then $N_G(U)\neq U$.
\end{cor}
\begin{thm}[1. Sylow Theorem\index{Sylow!1.Theorem}]
  Let $G$ be a finite group, with $\abs{G} = p^r\cdot m$ for a prime number $p$, $r\geq 0$ and $p\ndivd m$. Then
  \begin{enumerate}
    \item For all $0\leq i \leq r$, there is subgroup of $G$ of the order $p^i$.
    \item Every subgroup of $G$ with order $p^i$ for $0\leq i\leq r-1$ is a normal divisor of a subgroup of order $p^{i+1}$.
  \end{enumerate}
\end{thm}
\begin{proof}
\begin{enumerate}
\item
If $r=0$, then i) states that there is a subgroup of order $1$, i.e. the trivial group, which is true. So assume that $r\geq 1$. \par
We preceed with induction on $i$.
By Cauchys theorem (\cref{9:cauchy}), there is an element $g$ with $\ord g = p$, and so the cyclic group $\gby{g}$ is of order $p$. Assume now that there is a subgroup $U\leq G$ with $\abs{U}=p^i$ for $1\leq i \leq r-i.$ Then $[G:U]=p^{r-i}\cdot m$, and since $U$ is a $p$-group, \cref{10:nonormal} implies that $N_G(U)\neq U$.
So $N_G(U)/U$ is non-trivial (It is a group, as $U\nsg N_G(U)$). By \cref{9:primnormal}, $N_G(U)/G$ is also a $p$-group. So, again by Cauchys Theorem, $N_G(U)/U$ has a subgroup of order $p$. But this subgroup lifts to a subgroup
\[
U \leq U' \leq N_G(U)
\]
with $\ord(U')=p^{i+1}$.
\item For the above construction of $U'$, $U\nsg N_G(U)$ holds, so in particular $U\nsg U'$.
\end{enumerate}
\end{proof}
\begin{thm}[2. Sylow Theorem\index{Sylow!2. Theorem}]\label{10:2sylow}
  Let $G$ be a finite group, $U\leq G$ a $p$-group and $P\leq G$ a \psyl{}  group. Then
  \begin{enumerate}
    \item There is a $g\in G$, such that
    \[
    gUg^{-1}\leq P.
    \]
    \item Any two \psyl{} subgroups of $G$ are conjugate to each other.
  \end{enumerate}
\end{thm}
\begin{proof}
  \begin{enumerate}
  \item Set $X:= G/P$ and consider the group action
  \[
  \phi:\grpac{U}{X},~(U,gP)\mapsto (ug)P.
  \]
  The well-definedness of $\phi$ is the same as in the proof of \cref{9:primnormal}. By \cref{9:grpordercongr}. We have
  \[
  \abs{X}\equiv \abs{X} \modu{p}.
  \]
  As $P$ is a \psyl{}group, We know that $p\ndivd \abs{X}$, so $\abs{X^U}\neq 0$, and hence $X^U\neq \emptyset$. So there is a $g\in G$, such that
  \[
  ugP = gP\text{ for all }u\in U
  \]
  and hence $g^{-1}ug\in P$ for all $u\in U$.
  \item This follows from i), as \psyl{}groups are in particular $p$-groups.
  \end{enumerate}
\end{proof}

\begin{cor}
  Let $G$ be a finite group. Then any two \psyl{}groups of $G$ are isomorphic.
\end{cor}

\begin{cor}\label{10: sylow group normal iff the only one}
  Let $G$ be a finite group, $P\leq G$ a \psyl{}group of $G$. Then the following are equaivalent
  \begin{enumerate}
  \item $P$ is normal.
  \item $\abs{\sylp(G)} = 1$.
  \end{enumerate}
  In particular, if $G$ is abelian, then $\abs{\sylp(G)} =1$.
\end{cor}

\begin{thm}[3. Sylow\index{Sylow!3. Theorem}]
  Let $G$ be a finite group, $\abs{G}=p^r\cdot m$, with $p$ prime, $p\ndivd m$ and $r\geq 0$. Let $n_p := \abs{\sylp(G)}$. Then
  \begin{enumerate}
    \item $n_p \divd \abs{G}$.
    \item $n_p \equiv 1 \modu{p}$.
  \end{enumerate}

\end{thm}
\begin{proof}
  \begin{enumerate}
  \item Let $P\leq G$ be a \psyl{} group. Then from 2. Sylow (\cref{10:2sylow}), we know that
  \[
  n_p = \abs{\{gPg^{-1}\ssp g\in G\}}.
  \]
  Consider now
  \[
  X:= \{ U\leq G \ssp U\subseteq G\}
  \]
  and the group action
  \[
  \phi: \grpac{G}{X},~(g,H)\mapsto gHg^{-1}.
  \]
  For this action, $\abs{G.P} = n_P$.
  By Langrange, we know that $p\divd \abs{G/G_p}$, and by \cref{9:gacard} that $\abs{G/G_p} = \abs{G.p}$, so the claim follows.
  \item Set $Y:= \sylp(G)$ and consider the group action
  \[
  \phi: \grpac{P}{Y} \to Y,~ (g,H) \mapsto gHg^{-1}.
  \]
  Then the set of fixed points is given by
  \begin{align*}
    Y^P &= \{ Q \in Y\ssp gQg^{-1}= Q\text{ for all }g\in P\}\\
    &= \{Q\in Y\ssp P \leq N_G(Q)\}.
  \end{align*}
  Note that $Y^P\neq \emptyset$, as $P \in Y^P$.
  For $Q\in Y^P$, both $P$ and $Q$ are \psyl{} groups of $N_G(Q)$. By 2. Sylow (\cref{10:2sylow}), there is a $g\in N_G(Q)$, such that
  \[
  Q = gQg^{-1} = P.
  \]
  Hence $\abs{Y^P}=1$ So by \cref{9:grpordercongr},
  \[
  n_p = \abs{Y} \equiv \abs{Y^P} = 1\modu{p},
  \]
  as $P$ is a $p$-group.
  \end{enumerate}
\end{proof}
\begin{ncor}
  There is only one subgroup of order $15$.
\end{ncor}
\section{Solvable Groups}
\begin{defn}
  Let $G$ be a group. A chain of the form
  \[
  \gseries{G}
  \]
  is a \toidx{normal series} of $G$. The quotients $G_i/G_{i-1}$ are
  \emph{factors of the normal series}.\\
  $G$ is called \toidx{solvable}, if there is a normal series of $G$ such that all factors are abelian.
\end{defn}
\begin{bsp}
\begin{enumerate}
  \item Every abelian group is solvable ($1\nsg G$).
  \item $S_4$ is solvable, $S_5$ however not.
\end{enumerate}
\end{bsp}
\begin{thm}\label{10:solvprop}
Let $G$ be a solvable group.
\begin{enumerate}
  \item All subgroups of $G$ are solvable.
  \item Let $f:G\to H$ be a homomorphism of groups. Then $\im f$ is solvable.
\end{enumerate}
\end{thm}
\begin{proof}
Let
\[
\gseries{G}
\]
be a normal series with $\gchainf{G}$ abelian.
\begin{enumerate}
\item Consider the chain
\[
1 \leq (G_0\cap H)\leq (G_1\cap H)\leq \hdots \leq (G_n\cap H) = H
\]
Let $x\in G_{i-1}\cap H$ and $y\in G_i\cap H$. Then
\[
yxy^{-1}\in H\qquad \text{and}\qquad yxy^{-1}\in G_{i-1}\]
as $G_{i-1}\nsg G_i$. So $(H\cap G_{i-1}\nsg (H\cap G_i)$.\\
By 1. Nother, we have
\begin{align*}
(G_i\cap H)/(G_{i-1}\cap H) &= (G_i\cap H)/(G_i\cap G_{i-1}\cap H)\\
G_{i-1}(G_i\cap H)/G_{i-1} \leq G_i/G_{i-1}.
\end{align*}
But as $G_i/G_{i-1}$ is abelian, $(G_i\cap H)/(G_{i-1}\cap H)$ has to be abelian to.
\item Consider now the chain
\[
1 = f(G_0) \leq f(G_1) \leq \hdots \leq f(G_n) = \im f.
\]
Let $y\in G_i$ and $z = f(y) \in f(G_i)$. Then
\[
zf(G_i)z^{-1} = f(yG_{i-1}y^{-1}) = f(G_{i-1})
\]
as $G_{i-1}\nsg G_i$. So $f(G_{i-1})\nsg f(G_i)$.\\
Consider the composition
\[
\begin{tikzcd}
G_i\ar[\surarr]{r}{f}\ar[\surarr, bend left = 25]{rr}{f'}&f(G_i)\ar[\surarr]{r}&f(G_i)/f(G_{i-1})
\end{tikzcd}
\]
Then $G_{i-1}\leq \ker f'$, and hence we get a factorisation of the form
\[
\begin{tikzcd}
G_i \ar[swap,\surarr]{d}{f'}\ar{r}&G_i/G_{i-1}\ar[dashed,\surarr]{ld}{\overline{f}'}\\
f(G_i)/f(G_{i-1})
\end{tikzcd}
\]
\end{enumerate}
By the first isomorphism theorem, $\overline{f}'$ factors through its kernel, so we get
\[
\begin{tikzcd}
G_i \ar[swap,\surarr]{d}{f'}\ar{r}&G_i/G_{i-1}\ar[\surarr]{ld}{\overline{f}'}\ar{r}&(G_i/G_{i-1})/\ker \overline{f}'\ar[dashed, bend left = 15]{lld}{\cong}\\
f(G_i)/f(G_{i-1})
\end{tikzcd}
\]
But as $G_i/G_{i-1}$ is abelian, all of its quotients are, and hence $f(G_i)/f(G_{i-1})$ is too.
\end{proof}
\begin{defn}
  Let $G$ be a group. Set
  \[
  [G,G] := \gen{\{ghg^{-1}h^{-1}\ssp g,h\in G\}},
  \]
  the \toidx{commutator group} of $G$.
\end{defn}
\begin{lem}\label{10:comun}
The commutator is the smallest normal subgroup of $G$ such thtat the quotient is abelian.
\end{lem}
\begin{proof}
Let $g,h,s\in G$. Then
\begin{align*}
  s[g,h]s^{-1}&= sgs^{-1}shs^{-1}sg^{-1}s^{-1}sh^{-1}s^{-1}\\
  &= [sgs^{-1},shs^{-1}] \in [G,G].
\end{align*}
Now for any product
\[
s[g_1,h_1]\cdot [g_n,h_n]s^{-1} = s[g_1,h_1]s^{-1}s\cdot s^{-1}[g_n,h_n]s^{-1}
\]
so the commutator is indeed normal.\\
Now let $N\nsg G$. For all $g,h\in G$:
\begin{align*}
 (gN)(hN)= (hN)  (gN)
\iff (gh)N = (hg)N
\iff g^{-1}h^{-1}gh\in N,
\end{align*}
so $G/N$ is abelian if and only if $[G,G]\leq N$.
\end{proof}
\begin{defn}
Let $G$ be a group and $m\geq 0$. We set
\[
D^m (G):= \begin{cases}G&\text{if } m=0\\ [D^{m-1}(G),D^{m-1}(G)]&\text{else}\end{cases}.
\]
which are called the \toidx{derived groups} of $G$.
This leads to the chain
\begin{equation}\label{10:dchain}\tag{DChain}
  \hdots \nsg D^m(G)\nsg D^{m-1}(G)\nsg \hdots \nsg D^0(G)=G.
\end{equation}
\end{defn}
\begin{thm}
Let $G$ be a group. The following are equivalent:
\begin{enumerate}
\item $G$ is solvable.
\item There is a $m\geq 0$ such that $D^m(G)=1$.
\end{enumerate}
\end{thm}
\begin{proof}
i) $\implies$ ii): In this case, \eqref{10:dchain} is a resolution of $G$.\par
ii) $\implies$ i): Let
\[
\gseries{G}
\]
be a resolution of $G$, with $G_i/G_{i-1}$ abelian. Then by \cref{10:comun}, $[G_i,G_i]\leq G_{i-1}$. We now show by induction on $i$ that
\[
D^{i}(G) \leq G_{n-i}.
\]
For $i=0$, $D^0(G) = G  = G_n$ holds; and for $i=1$, $D^1(G) = [G,G] \leq [G,G]_{n-1}$. By induction, we get $[D^i(G),D^i(G)] \leq [G_{n-i},G_{n-i}]$, and hence using the above observation, we have
\begin{align*}
D^{i+1}(G) &= [D^i(G),D^i(G)]\\
  &\leq [G_{n-i},G_{n-i}]\\
  &\leq G_{n-i-1}
\end{align*}
So in particular, $D^n(G) \leq G_0 = 1$.
\end{proof}
\lec
