In this lecture, $K$ denotes a field.
\begin{prop}
  Let $f\in K[X]$ be a polynomial over $K$, with finite degree $n:= \deg f\geq 0$. Then the quotient ring $K[X]/(f)$ is a $n$-dimensional $K$-vector space and a basis is given by
  \[
  B:= \{ X^i + (f)\ssp 0\leq i\leq n-1\}
  \]
\end{prop}
\begin{proof}
  A basis of $K[X]$ is given by $\{ X^i\ssp i\geq 0\}$, so the set $\{ X^i+(f)\ssp i\geq 0\}$ generates $K[X]/(f)$, as the quotient map is surjective. With this information, we show the claim in two steps:\par
   $X^n +(f)\in \lin(B)$: There are $a_0,...,a_n\in K$, such that
   \[
   f = a_nX^n+\underbrace{a_{n-1}X^{n-1}+...+a_0}_{=: h},
   \]
   with $a_n \neq 0$.
  So
  \[
  X^n + \frac{1}{a_n}h = \frac{1}{a_n}f \in (f)\implies X^n + (f) = -\frac{1}{a_n}h+(f).
  \]
  As $\deg h \leq n-1$, $X^n+(f)\in \lin(B)$ follows. By induction, we get $X^j+(f)\in \lin(B)$ for all $j\in \nn$. So, in particular, we have
  \[
  K[X]/(f) = \lin(B).
  \]
  \par
  $B$ is linearly independent: For all $0\neq h\in (f)$, $\deg h \geq \deg f $ holds. Now assume that there are $b_0,\hdots, b_{n-1}\in K$, which are not all zero, such that
  \[
  b_0 (1+(f)) + \hdots b_{n-1}(X^{n-1}+(f)) = (f).
  \]
  Then
  \[
  h:=0 \neq b_0+\hdots + b_{n-1}X^{n-1}\in (f)\qquad \text{but}\qquad \deg h\leq n-1 < n = \deg f,
  \]
  so this is not possible.

\end{proof}
\begin{rem}
  Let $0\neq f \in K[X]$. Then the composition
  \[
  \begin{tikzcd}
    K\ar[\embarr]{r}&K[X]\ar[\surarr]{r}&K[X]/(f)
  \end{tikzcd}
  \]
  is an injective ring homomorphism, as $1\mapsto 1+(f) \neq (f)$. So we can regard $K$ as a subring of the quotient $K[X]/(f)$.
\end{rem}
\begin{defn}
  Let $L/K$ be a field extension, and $a\in L$. Set
  \[
  K[a]:= \sum_{i\geq 0}Ka^i :=\bigg  \{ \sum_{i=0}^n\lambda a^i\ssp n\in \nn, \lambda \in K\bigg \}.
  \]
  This is a subring of $L$, but not necessarily a subfield. In particular, $K[a]$ and $K(a)$ are not necessarily equal.
\end{defn}
\begin{lem}\label{14:polyquot2}
  Let $L/K$ be a field extension, and $a\in L$ an algebraic element of $L/K$. Let $f:=\mip{a}{K}$ be the minimal polynomial of $a$.
  \begin{enumerate}
  \item The evaluation morphism
  \[
    \ev_a:K[X]\to K(a), f\mapsto f(a)
  \]
  is surjective.
  \item $\ker(\ev_a)=(f)$ holds, and in particular $K(a)\cong K[X]/(f)$.
  \item Let $n:= \deg f$. Then
  \[
  K(a) = K[a] = \sum_{i=0}^{n-1}Ka^i.
  \]
  \end{enumerate}
\end{lem}
\begin{proof}
  \begin{enumerate}
    \item By \cref{3:febasis}, the set $\{1,\hdots,a^{n-1}\}$ is a $K$-basis of $K(a)/K$. As $\ev_a(X^i)=a^i$, we get (by linearity) the surjectivity of $\ev_a$.
    \item By the minimality of $\mip{a}{K}$, we get
    \[
    (f) = (\mip{a}{K}) = \{ g\in K[X]\ssp g(a) = 0\} = \ker (\ev_a).
    \]
    \item If $a=0$, we have $K[0]=0=K(0)$, so assume $a\neq 0$.
    We always have that $K[a]\sse K(a)$. But, in addition, $\lin \{1,\hdots,x^{n-1}\}\sse K[a]$, so the claim follows.
  \end{enumerate}
\end{proof}

\section{Factorial Rings}
In this section, all rings are assumed commutative.
\begin{defn}
  Let $R$ be an integral domain, and $a\in R$.
  \begin{enumerate}
    \item We say $a$ is \toidx{irreducible}, if $a\neq 0$, $a\notin \unts{R}$ and for all $b,c\in R$ with $a =bc$ already $b\in \unts{R}$ or $c\in \unts{C}$ follows.
    \item We say $a$ is \toidx{prime}, if $a\neq 0$, $\notin \unts{R}$ and for all $b,c\in R$ with $a\divd bc$ already $a\divd b$ or $a\divd c$ follows.
  \end{enumerate}
\end{defn}
\begin{defn}
  Let $R$ be an integral domain. $R$ is a \toidx{factorial} (\textit{faktoriell}) if for all $0\neq a\in R$,
  \begin{enumerate}
    \item there is a product decomposition
    \[
    a = e p_1\hdots p_m\qquad \text{with~}e\in \unts{R},p_1,\hdots ,p_m\in R\text{ irreducible}.
    \]
    Such a decomposition is a \toidx{decomposition into irreducibles} (\textit{Primfaktorzerlegung}).
    \item and two decomposition
    \[
    a = ep_1\hdots p_m = fq_1\hdots q_n
    \]
    with $p_1,\hdots,p_m,q_1,\hdots,q_n\in R$ irreducible and \coms$ e,f\in \unts{R}$\come, already $m=n$ holds and there are $e_0,\hdots,e_m\in \unts{R}$ and a bijection $\sigma\in S_m$ such that $e_oe=f$ and  $e_ip_i = q_{\sigma(i)}$.
    \end{enumerate}
\end{defn}
\begin{defn}
  Let $S$ be a ring and $R\sse S$ a subring. Let $M\sse S$ be any subset. We set
  \[
  R[M]:= \bigcap_{\substack{R\sse U\sse M,\\M\sse U,\\U\text{ subring}}} U.
  \]
  Then $R[M]\sse S$ is a subring of $S$. For a finite set $M:= \{x_1,\hdots,x_n\}$, we set $R[x_1,\hdots,x_n]:= R[M]$.
\end{defn}

\begin{bsp}
  Consider
  \[
  R:= \zz[\sqrt{-5}] = \{ a+bi\sqrt{5}\sse a,b\in \zz\}.
  \]
  As $R$ is a subring of $\cc$, it follows that $R$ is an integral domain. In this ring, $2,3,1+i\sqrt{5},1-i\sqrt{5}$ are all irreducible. So we get
  \[
  2\cdot 3 = 6 =(1+i\sqrt{5})(1-i\sqrt{5}),
  \]
  so $R$ is not factorial.
\end{bsp}

\begin{lem}\label{14:primelid}
  Let $R$ be an integral domain, and $a\in R$. Then
  \begin{enumerate}
    \item $a$ is prime if and only if $0\subsetneq (a)$is a prime ideal.
    \item $a$ is irreducible if and only if $(0)\subsetneq (a)\subsetneq  R$ and for all $b\in R$ wiht $(a)\sse (b)$ already $(a)=(b)$ or $(b)=R$ follows.
  \end{enumerate}
\end{lem}
\begin{proof}
  \begin{enumerate}
    \item Let $a\in R$ be prime. Then $0\neq a$ and $a\notin \unts{R}$. So $(0)\subsetneq (a)\subsetneq R$. Let $b,c\in R$ with $bc\in (a)$. Then $a\divd bc$, and as $a$ is prime, $a\divd b$ or $a\divd c$ follows. So $b\in (a)$ or $c\in (a)$. \par
    Let $0\subsetneq (a)$ be a prime ideal, so $(a)\subsetneq R$, and hence $a\notin \unts{R}$. Assume that $a\divd bc$, for some $b,c\in R$. Then $bc\in (a)$, and hence $b\in (a)$ or $c\in (a)$. So $a\divd b$ or $a\divd c$.
    \item Assume $a$ is irreducible. Then $(0)\subsetneq (a)\subsetneq R$ follows, as $a\neq 0,a\notin \unts{R}$. Let $b\in R$ with $(a)\sse(b)$. Then $b\divd a$, and hence there is $r\in R$ with $br = a$. As $a$ is irreducible, $r\in \unts{R}$ or $b\in \unts{R}$ follows. If $r\in \unts{R}$, $(b)=(a)$ follows.
    If $b\in \unts{R}$, then $(b)=R$.\par
    Assume $(a)$ satisfies these properties. Then $a\neq 0,a\notin\unts{R}$ follows. Let $b,c\in R$ with $bc=a$. Then $(a)\sse (b)$ holds. By assumption, $(a)=(b)$ or $(b)=R$ follows. If $(b)=R$, then $b$ is a unit. If $(a)=(b)$, there is a $r\in R$ with $b = ar$. So
    \[
    a = bc = arc \implies a(1-rc)=0\overset{a\neq 0}{\implies} 1-rc = 0\implies c\in \unts{R}.
    \]
  \end{enumerate}
\end{proof}
\begin{lem}\label{14:primetoid}
  Let $R$ be an integral domain. If $a\in R$ is prime, than $a$ is irreducible.
\end{lem}
\begin{proof}
  Let $b,c\in R$ with $a=bc$, so $a\divd bc$. As $a$ is prime, we can assume $a\divd b$. Hence there is $r\in R$ with $b = ar$. Then
  \[
  a = bc = arc \implies a(1-rc)=0\implies c\in \unts{R}.
  \]
\end{proof}
\begin{ncor}
  Let $R$ be a principal ideal domain. Then every prime ideal is maximal.
\end{ncor}
\begin{lem}\label{14:idtoprime}
  Let $R$ be factorial. If $a\in R$ is irreducible, then $a$ is prime.
\end{lem}
\begin{proof}
  Assume there $b,c\in R$ with $a\divd bc$. Then there is a $d\in R$ with $ad = bc$, and $d_1,\hdots,d_r,b_1,\hdots,b_s,c_1,\hdots,c_t\in R$ irreducible and \coms$ e_1,e_2,e_3\in \unts{R}$\come{} such that
  \[
  d = e_1d_1\hdots d_r,~b = e_2b_1\hdots b_s,~ c = e_3c_1\hdots c_t
  \]
  and
  \[
  ad_1\hdots d_r =(e_1^{-1}e_2e_3) b_1\hdots b_sc_1\hdots c_t.
  \]
  Hence there is a unit $e\in \unts{R}$ with $ea = b_i$ or $ea = c_i$ for a given $i$. Hence $a\divd b_i$ or $a\divd c_i$, and so $a\divd b$ or $a\divd c$.
\end{proof}
\lec
