\begin{lem}\label{17:quotint}
  Let $R$ be an integral domain, and $S\sse R$ a multiplicativley closed subset with $0\notin S$. Then $S^{-1}R$ is an intgeral domain.
\end{lem}
\begin{proof}
  Assume $(0,1)\sim (1,1)$. Then there is a $s\in S$ such that
  \[
  0 = s\cdot (0\cdot 1 - 1\cdot 1) = -s,
  \]
  which is not possible. Hence $1/1\neq 0/1$, so $S^{-1}R$ is not the zero-ring.\par
  Let $a/b,c/d\in S$. Then
  \[
  a/b\cdot c/d = (ac)/(bd)=(ca)/(db) = c/d\cdot a/b.
  \]
  Hence $S^{-1}R$ is commutative. Assume furthermore that $a/b\cdot c/d = 0$. Then $0/1 = (ac)/(bc)$, so there is a $s\in S$ with
  \[
  0 = s\cdot(ac\cdot 1 - bd\cdot 0) = sac.
  \]
  As $R$ is an integral domain and $s\neq 0$, we get $a=0$ or $c=0$. So $a/b=0$ or $c/d=0$.
\end{proof}

\begin{defn}
  Let $R$ be an intgeral domain, and $S:= R\setminus \{0\}$. Then
  \[
  \quot R := S^{-1}R
  \]
  is the \toidx{quotient field} of $R$.
\end{defn}
\begin{lem}
  The quotient field $\quot R$ is indeed a field.
\end{lem}
\begin{proof}
  As $R$ is an integral domain, $S$ is indeed multiplicativley closed. By \cref{17:quotint}, $\quot R$ is an integral domain. Furthermore, for $0\neq a/b$, $a\neq 0$ follows, and hence $b/a\in \quot R$, so
  \[
  a/b\cdot b/a = 1/1 = 1,
  \]
  so $a/b\in \unts{\left(\quot R\right) }$ follows.
\end{proof}
\begin{thm}[Universal property of the Localisation]
Let $S\sse R$ multiplicativley closed.
\begin{enumerate}
  \item The map
  \[
  i_S: R\to S^{-1}R,~r\mapsto r/1
  \]
  is a ring homomorphism, and $i_S(S)\sse \unts{\left(S^{-1}R\right)}$.
  \item $S^{-1}R$ is universal with this property, i.e. for any other ring homomorphism $f:R\to S$, such that $f(S)\sse \unts{T}$, there is a unique ring homomorphism $\hat{f}:S^{-1}R\to T$, such that the diagram
  \[
  \begin{tikzcd}
    R\ar{r}{f}\ar{d}[left]{i_S}&T\\
    S^{-1}R\ar[dashed]{ur}[below right]{\hat{f}}&
  \end{tikzcd}
  \]
  commutes.
\end{enumerate}
\end{thm}
\begin{proof}
  \begin{enumerate}
    \item We have $i_S(1)=1/1=1_{S^{-1}R}$,
    \[
    i_S(a+b) = (a+b)/1 = a/1+b/1\]
     and
     \[
     i_S(ab)=(ab)/1=a/1\cdot b/1.
     \] Furthermore, for $s\in S$, $s/1\cdot 1/s=1/1=1_{S^{-1}R}$ holds, so $i_S(S)\sse \unts{\left(S^{-1}R\right)}$ holds.
    \item Let $r/s\in S^{-1}R$. Consider the map
    \[
    \hat{f}:S^{-1}R\to T, r/s\mapsto f(r)f(s)^{-1}.
    \]
    This makes sense, as $f(s)\in \unts{T}$ by assumption. Assume furthermore that $a/b = c/d$. Then there is a $s\in S$ such that $0 = s(ad-bc)$. Then
    \begin{align*}
    0 &= f\left(s(ad-bc)\right) \\
    \implies 0&=  f(s)\cdot \left(f(a)f(d) - f(b)f(c)\right)\\
    \implies 0 &= f(a)f(d) - f(b)f(c)\\
    \implies f(a)f(b)^{-1} &= f(c)f(d)^{-1},
    \end{align*}
    where we first used $f(s)\in \unts{T}$ and then $f(b),f(d)\in \unts{T}$, and that $f$ is ring homomorphism. So $\hat{f}$ is well-defined. That $\hat{f}$ is a ring homomorphism can be checked by direct calculation.\par
    The diagram is commutative, as
    \[
    \left(\hat{f}\circ i_S\right)(r) = \hat{f}(r/1) = f(r)f(1)^{-1}=f(r)\]
    as $f$ is a ring homomorphism.\par
    To show the uniqueness of $\hat{f}$, we assume that $g:S^{-1}R\to T$ is another ring homomorphism such that $g\circ i_S=f$. Then $g(r/s) = g(r/1)g(1/s)$ and
    \begin{align*}
    g(r/1)g\left(s/1\right)^{-1})&= g(r/1)g(s/1)^{-1}\\
    &= g(i_S(r))g(i_S(s))^{-1}\\
    &= f(r)f(s)^{-1} = \hat{f}(r/s)
    \end{align*}
  \end{enumerate}
\end{proof}
\begin{lem}
  Let $S\sse R$ be a multiplicativley closed subset. Assume that $S$ does not any zero-divisors. then $i_S: R\to S^{-1}R$ is injective.
\end{lem}
\begin{proof}
  Assume $i_S(r)=0$. Then $r/1=0/1$, so there exists a $s\in S$, such that
  \[
  0 = s(r\cdot 1-1\cdot 0)\implies sr = 0\implies r = 0
  \]
  as $S$ does not contain zero-divisors. So $\ker i_S$ is trivial, and hence $i_S$ injective.
\end{proof}
\section{Gauss' Theorem}
In this section, $R$ denotes a \textit{factorial ring}.
\begin{defn}
  Let $f = a_nX^n+\hdots + a_0\in R[X]$ be a polynomial with $\deg f = n$. We say $f$ is \toidx{primitive} (\textit{primitiv}\footnote{ It seems like this terminology is not commonly used, and I am not aware of the right translation.}) if there is no $r\in R\setminus \unts{R}$ such that $r\divd a_i$ for all $0\leq i \leq n$ (i.e. $r\divd f$ in $R[X]$, with $\notin \unts{R}$).
\end{defn}

\begin{thm}[Gauss]\label{17:gauss}
  Let $R$ be a factorial ring. Then $R[X]$ is also factorial. Furthermore, the irreducible elements in $R[X]$ are
  \begin{enumerate}
    \item irreducible elements in $R$ (where we regard $R$ as subring), and
    \item the primitive polynomials $f\in R[X]$, such that $f$ is irreducible in $\quot(R)[X]$.
  \end{enumerate}
  Here we regard $R[X]$ as a subring of $\quot(R)[X]$ using the embedding $R\membarr \quot(R)\membarr \quot(R)[X]$.
\end{thm}
An immediate consequence is
\begin{cor}
  The rings $\zz[X_1,\hdots,X_n]$ and $K[X_1,\hdots,X_n]$ are factorial, for $n\in \nn$ and $K$ a field.
\end{cor}
To show the claim, we first need to show some lemmata:\par
\begin{lem}[Gauss Lemma]\label{17:gausslemma}
  If $P,Q\in R[X]$ are primitive, then $PQ$ is primitive as well.
\end{lem}
\begin{proof}
  Assume that $PQ$ is not primitive, so there is a $r\in R\setminus \unts{R}$ such that $r\divd PQ$. As $R$ is factorial, we can assume that $r$ is a prime element. So $(r)\sse R$ is a prime ideal, by \cref{14:primelid}, and hence $S:= R/(r)$ is an integral domain. We can consider now the induced ring homomorphism
  \[
  \overline{\cdot}:R[X]\to S[X]\qquad \text{with}\qquad X\mapsto (1+(r))\cdot X.
  \]
  As $r\divd PQ$, we have $\overline{PQ} = 0$ in $S[X]$. So $\overline{P}=0$ or $\overline{Q}=0$, as $S$ being an intgral domain implies that $S[X]$ is an integral domain too. Hence $r\divd P$ or $r\divd Q$, so $P$ or $Q$ is not primitive, contrary to the assumption.
\end{proof}
\begin{lem}\label{17:primirr}
  Let $R$ be factorial, $f\in R[X]$ primitive, and set $K:= \quot(R)$. If $f$ is irreducible in $K[X]$, then $f$ is also irreducible in $R[X]$.
\end{lem}
\begin{proof}
  Assume there is a decomposition $f=bc$ in $R[X]$. This is also a factorisation in $K[X]$. As $f$ is irreducible in $K[X]$, $b\in \unts{K[X]}=\unts{K}$ or $c\in \unts{K[X]}$ follows. Assume $b\in \unts{K}$. As $b\in R[X]$ and $b\in \unts{K}$, we have $b\in R\setminus \{0\}$
  (as $b\in \unts{K} = \{x/y\ssp x,y\in R\setminus\{0\}\}$ and $b\in R[X]\implies b = b/1\in K[X]$). As $R$ is factorial, there is a factorisation $b=e_1p_1\hdots p_t$ with $e_1\in \unts{R}$ and $p_1,\hdots, p_t$ irreducible in $R$, and in particular $p_i\in R\setminus \unts{R}$.
  But for every $p_i$ in the decomposition of $b$ into irreducibles, $p_i \divd f$ holds (as $b\divd f$). As $f$ is primitive, there can be no such elements in $R\setminus \unts{R}$. Hence $b=e\in \unts{R}$, which shows that $f$ is irreducible.
  \end{proof}
  %added kater by me
  \begin{lem}
    Let $R$ be a factorial ring and $K:= \quot(R)$. Then for every $f\in K[X]$, there is a $c\in \unts{K}$, such that $\overline{f}:= cf$ is an element of $R[X]\sse K[X]$, and $\overline{f}$ is primitve.
  \end{lem}
  \begin{proof}[Proof of \cref{17:gauss}]
  \leavevmode
    \begin{itemize}
      \item The elements in i) and ii) are \textit{irreducible}: Assume $r\in R$ is irreducible, and that there is a factorisation $r = bc$ in $R[X]$. As $R$ is an integral domain, $R[X]$ is too, and hence $\deg b,\deg c = 0$, so $b,c\in R$. As $r$ is irreducible in $R$, $b\in \unts{R}$ or $c\in \unts{R}$ follows. As $\unts{R}\sse \unts{R[X]}$, this implies that $r$ is irreducible in $R[X]$, so i) holds. Claim ii) is \cref{17:primirr}.
      \item \textit{Existence} of the decomposition into irreducibles: Let $K:= \quot(R)$. then $K[X]$ is factorial. Let $0\neq f \in R[X]\sse K[X]$. Then there is a decomposition into irreducibles of $f$ in $K[X]$, say $f = e_1p_1\hdots p_t$ with $e\in \unts{K[X]} = \unts{K}$ and $p_i$ irreducible in $K[X]$.For all $1\leq i \leq t$, there is a $c_i\neq 0$ such that $p_i = c_i \overline{p_i}$ with $\overline{p_i}$ irreducible in $R[X]$.
    \end{itemize}
  \end{proof}

\lec
