\section{Perfect Fields}
Let $K$ be a field.
\begin{defn}
  We say a field $K$ is \emph{perfect}\index{field!perfect} if every $f\in K[X]$ is separable.
\end{defn}

\begin{lem}\label{21:perfect iff every algebraic separable}
  The following are equivalent:
  \begin{enumerate}
    \item $K$ is perfect.
    \item Every algebraic field extension $L/K$ is separable.
  \end{enumerate}
\end{lem}

\begin{proof}
i) $\implies$ ii): As the minimal polynomial of every $x\in L$ is an element in $K[X]$, $K$ being perfect implies that $L/K$ is separable.\par
ii) $\implies$ i): Let $f\in K[X]$ be irreducible. Without loss of generality, we can assume that $f$ is monic. Let $a\in \kk$ be a root of $f$ in the algebraic closure $\kk$. Then $K(a)/K$ is an algebraic extension, and hence by assumption separable. So $a$ is separable, and hence $\mip{a}{K}=f$ is separable.
\end{proof}

\begin{bsp}
  \begin{enumerate}
    \item Assume $K$ has characteristic zero. Then by \cref{20:sepcond}, $K$ is perfect.
    \item Assume $K=\kk$. Then $K$ is perfect, as the irreducible polynmoials are exactly those of degree $1$.
  \end{enumerate}
\end{bsp}

\begin{lem}
  Assume $\rchar K = p>0$. Then the following are equivalent:
  \begin{enumerate}
    \item $K$ is perfect.
    \item $K=K^p$, i.e. the Frobenius homomorphism is surjective.
  \end{enumerate}
\end{lem}
\begin{proof}
  i) $\implies$ ii): Let $a\in K\setminus K^p$. Let $b\in \kk$ be a root of $X^p-a\in K[X]$. Hence $a= b^p$, and (as $\rchar K=p$) $f = (X-b)^p$. We also have $b\notin K$, as $a \notin K^p$. Therefore $[K(b):K]\geq 2$. Hence for $g:= \mip{b}{K}$, we have $g\divd f$, so $g = (X-b)^m$, with $m\geq 2$, (in the case $m=1$, $b\in K$ would follow).
  But then $g$ would be inseparable and irreducible, contrary to the assumption that $K$ is perfect.\par
  ii) $\implies$ i): Let $f\in K[X]$ be irreducible and inseparable. Then by \cref{20:sepcond}, we have
  \[
  f = \sum_{j\geq 0}a_j \left(X^{(p^e)}\right)^j
  \]
  with $e>0$ and $a_j \in K$. As $K=K^p=K^{(p^2)}=\hdots = K^{(p^e)}$, there are $b_j \in K$, such that $a_j= \left(b_j\right)^{(p^e)}$. Hence
  \begin{align*}
    f &= \sum_{j\geq 0}a_j \left(X^{(p^e)}\right)^j\\
    &= \sum_{j\geq 0}b_j^{(p^e)}\left(X^{p^e}\right)^j\\
    &= \left(\sum_{j\geq 0}b_j X^j\right)^{(p^e)},
  \end{align*}
  which contradicts the assumption that $f$ is irreducible.
\end{proof}
\begin{cor}\label{21: finite implies perfect}
  If $K$ is finite, then $K$ is perfect.
\end{cor}
\begin{proof}
  By \cref{20:frob}, we have $K= K^p$, for $p = \rchar K>0$.
\end{proof}
\begin{bsp}
  Assume $\rchar K>0$. Then for $L:= \quot\left(K[T]\right)$, we have $L^p = K^p\left(T^p\right)$, and as $T\notin K^p\left(T^p\right)$, $L^p\neq L$, so $L$ is not perfect.
\end{bsp}

\section{Primitive Elements}
\begin{defn}
  Let $L/K$ be a finite field extension. We say $a\in L$ is a \toidx{primitive element} for $L/K$, if $L=K(a)$ holds.
\end{defn}
\begin{thm}[Primitive Element Theorem]\label{21: finite and sep has primitive}\index{theorem! primitive element}
  Let $L/K$ be a finite and separable field extension. Then there exists a primitive element for $L/K$.
\end{thm}
\begin{proof}
  \begin{itemize}
    \item Assume $K$ is \textit{finite}. Then $L$ is also finite, as it $L/K$ is finite. By \cref{8:cycunits}, $\unts{L} = \langle a \rangle$. So $L = K(a)$, and hence $a$ is a primitive element for $L/K$. (It was not necessary that $L/K$ is separable in this case).
    \item Assume $K$ is \textit{infinite}. As $L/K$ is algebraic, there are $a_1,\hdots,a_n\in L$ such that $L = K(a_1,\hdots,a_n)$. Using induction, it suffices to show that for $L=K(b,c)$ there is a $a\in L$ such that $L= K(a)$
  \end{itemize}
  \coms to be continued\come.
\end{proof}

\begin{bsp}
    Let $K$ be a field with characteristic $\rchar K = p>0$. Let $L:= K (X_1,X_2):= \quot \left(K[X_1,X_2]\right)$, and $Z:= K\left(X_1^p,X_2^p\right)$. Then $[L:Z]=p^2$, and for all $f\in L$ we have $f^p \in Z$. Assume $f\in L$ is a primitive element for $L/Z$. Then $\deg \mip{f}{Z}=p^2$. But $f$ is a root of $X^p-f^p\in Z[X]$. So there is no primitive element for $L/Z$.
    This shows that $L/K$ is not separable.
\end{bsp}
\begin{cor}
  Let $K$ be a perfect field, and $L/K$ a finite field extension. Then there is a primitive element for $L/K$.
\end{cor}
\begin{proof}
  As $L/K$ is finite, it is in particular algebraic. So by \cref{21:perfect iff every algebraic separable}, we have that $L/K$ is separable. So by \cref{21: finite and sep has primitive}, it follows that there is a primitive element for $L/K$.
\end{proof}

\chapter{The Fundamental Theorem of Galois Theory}
Let $L/K$ be a field extension. The Galois-Group of this field extension was defined as
\[
\gal(L/K):= \lset K\text{-automorphisms }L\to L\rset.
\]
We now want to establish a correspondence between subgroups of $\gal (L/K)$ and intermediate field $K\sse Z\sse L$.
\begin{remdef}
\leavevmode
\begin{enumerate}
\item Let $H\leq \gal(L/K)$ be a subgroup. We call
\[
L^H:= \lset a\in L\ssp f(a)=a\text{ for all }h\in H\rset
\]
the \toidx{fixed field} of $H$. This is a intermediate of $L/K$.
\item Let $Z$ be a intermediate field of $L/K$. Then $\gal(L/Z)$ is a subgroup of $\gal(L/K)$.
\end{enumerate}
\end{remdef}
So there are maps
\[
\begin{tikzcd}
  \lset \text{subgroups of }\gal(L/K)\rset\ar[shift right]{d} &
  &
  H\ar[mapsto]{d}&
  \gal(L/Z)\ar[mapsfrom]{d}
  \\
  \lset \text{intermediate fields of }L/K\rset\ar[shift right]{u} &
  &
  L^H
  &Z
\end{tikzcd}.
\]
We want to find suitable conditions such that these maps are in bijection. In general, this is not true. Consider for example the field extension $\qq(\sqrt[3]{2})/\qq$. Then the Galois group is trivial, but there are exactly two indermediate fields.
\begin{defn}
  Let $L/K$ be a field extension. We say $L/K$ is a \toidx{Galois-extension} if $L/K$ is finite, normal and separable.
\end{defn}
\begin{goal}
  Let $L/K$ be a Galois-extension. Then  the maps above are inverse to each other.
\end{goal}
This will take some time.
\begin{lem}
  Let $L/K$ be a separable field extension. Then the following are equivalent:
  \begin{enumerate}
    \item $L/K$ is a Galois-extension.
    \item $L$ is a splitting field of $f\in K[X]$.
  \end{enumerate}
\end{lem}
\begin{proof}
  \coms Easy, but the claim normal iff algebraic and splitting field of a polynomial is still missing in the script.\come
\end{proof}
\begin{bsp}
  Let $f=X^4-2\in \qq[X]$. Then $\qq(\sqrt[4]{2},i)/\qq$ is a Galois-extension.
\end{bsp}
\lec
