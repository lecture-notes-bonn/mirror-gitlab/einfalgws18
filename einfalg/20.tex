\section{Separable Field Extensions}
\begin{defn}
  Let $L/K$ be a field extension. We say $f\in K[X]$ has a \emph{repeated root} in $L$, if there is an $a\in L$ and a $g\in L[X]$ such that $f = (X-a)^2\cdot g$.
\end{defn}

\begin{defn}
  \begin{enumerate}
    \item Let $f\in K[X]$ be irreducible. We say $f$ is \toidx{separable} over $K$ if $f$ has no repeated roots in $\kk$.
    \item Let $f\in K[X]$ be arbitrary. We say $f$ is separable over $K$ if all irreducible factors of $f$ are separable over $K$. Otherwise, $f$ is called \toidx{inseparable}.
    \item Let $L/K$ be an algebraic field extension. We say $a\in L$ is separable over $K$ if the minimal polynomial $\mip{a}{K}$ is separable. The field extension $L/K$ is separable if every $a\in L$ is separable over $K$. Otherwise, we say $L$ is inseparable.
  \end{enumerate}
\end{defn}

\begin{thm}\label{20:sep1}
  Let $L/K$ be a separable field extension, and $Z$ a subfield of $L/K$. Then both $L/Z$ and $Z/K$ are separable.
\end{thm}

\begin{proof}
  \begin{itemize}
    \item As $Z\sse L$, every polynomial in $Z[X]$ is also in $L[X]$, and hence separable over $K$.
    \item Let $a\in L$, $f:= \mip{a}{K}$ and $g:= \mip{a}{Z}$. As $K\sse Z$, we can regard $f$ as an element of $Z[X]$. By minimality of $g$ in $Z[X]$, we have $g\divd f$ in $Z[X]$. As $L/K$ is algebraic, $L/Z$ is too, and hence we can identify $\overline{K}$ and $\overline{Z}$.
    As $f$ is separable over $K$, $g$ has to be separable over $Z$.
  \end{itemize}
\end{proof}

\begin{thm}\label{20: separable and number of morphisms}
  Let $L/K$ be a field extension with $[L:K]=n$.
  \begin{enumerate}
    \item There are at most $n$ $K$-homomorphism $L\to \kk (= \overline{L})$.
    \item $L/K$ is separable if and only if there are \textit{exactly} $n$ $K$-homomorphism $L\to \kk$.
  \end{enumerate}
\end{thm}
\begin{proof}
  As $L/K$ is finite, there are finitely many $a_1,\hdots,a_t$ such that $L = K(a_1,\hdots,a_t)$. Set $L_0:= K$; $L_i := K(a_1,\hdots,a_i)$ and $n_i:= [L_i:L_{i-1}]$ for $1\leq i \leq t$. By the degree formular we have
  \[
  n = \prod_{i=1}^t n_i.
  \]
  \begin{enumerate}
  \item  We will prove this by induction on $i$: For $i=1$, $\mip{a_1}{K}$ has at most $n_1$ distinct roots in $\kk$, so there at most $n_1$ lifts of the form
    \[
    \begin{tikzcd}
      L_{i+1}\ar[dashed]{r}\ar[dash]{d}&
      \kk\ar[dash]{d}\\
      L_i\ar{r}\ar[dash]{d}&
      \kk\ar[equal]{u}\\
      K\ar[equal]{r}&
      K\ar[dash]{u}
    \end{tikzcd}.
    \]
    Continuing inductivley, the result follows.
    \item Assume $L/K$ is separable. By \cref{20:sep1}, we have that $L_{i+1}/L_i$ is separable. Hence $f:=\mip{a_{i+1}}{L_i}$ has exactly $n_{i+1}$ roots in $\overline{L}_i$. In addition, for every $K$-homomorphism $\phi:L_i\to \kk$, $\phi(f)$ has also exactly $n+1$ roots in $\kk$. So there are exactly $n_{i+1}$ lifts to a $K$-homomorphism $L_{i+1}\to \kk$.
    Continuing inductivley, the claim follows. \par
    Assume $L/K$ is inseparable. Without loss of generality, we can assume that $a_1$ is inseparable over $K$. Again by \cref{18:artin}, there are now less than $n_1$ K-homomorphisms $L_1\to \kk$.
  \end{enumerate}
\end{proof}
\begin{cor}\label{20: upper bound for galois group}
  Let $L/K$ be a finite field extension, with $[L:K]=n$. Then $\abs{\gal(L/K)}\leq n$.
\end{cor}
\begin{proof}
  We have
  \[
  \gal(L/K)\sse \fkcat (L,\overline{L}) \cong \fkcat (L,\kk),
  \]
  and $\abs{\fkcat(L,\kk)}\leq n$, by the preceeding theorem. Here we used that we can identify $\overline{K}$ and $\overline{L}$.
  \coms Here $\fkcat$ denotes the category of field extensions of $K$, where morphisms are $K$-homomorphisms.\come
\end{proof}
\begin{cor}
  Let $L/K$ be finite with $[L:K]=n$ and $M/K$ algebraic. Then $\abs{L,M}\leq n$.
\end{cor}
\begin{cor}\label{20:stepwisesep}
  Let $L = K(a_1,\hdots,a_t)$ such that $a_i$ is separable over $K(a_1,\hdots,a_{i-1})$ for $2\leq i \leq t$. Then $L/K$ is separable.
\end{cor}
\begin{cor}\label{20:compsep}
  Let $L/K$ be finite and $Z_1,Z_2$ intermediate fields of $L/K$:
  \[
  \begin{tikzcd}
  &
  L&
  \\
  &
  Z_1.Z_2\ar[dash]{u}&
  \\
  Z_1\ar[dash]{ur}&
  &
  Z_2\ar[dash]{ul}\\
  &
  K\ar[dash]{ur}\ar[dash]{ul}&
\end{tikzcd}.
\]
\end{cor}
\begin{proof}
  This follows from \cref{20:stepwisesep}.
\end{proof}
\begin{cor}
  Let $L/K$ and $M/L$ be separable field extensions:
  \[\begin{tikzcd}
    M\ar[dash]{d}\\
    L\ar[dash]{d}\\
    K
  \end{tikzcd}.\]
  Then $M/K$ is separable.
\end{cor}
\begin{proof}
Let $a\in M$, and $\mip{a}{L}= X^n+a_{n-1}X^{n-1}+\hdots+a_0\in L[X]$. Consider $L':=K(a_0,\hdots,a_{n-1})\sse L$. As $L/K$ is separable, $L'/K$ is too, by \cref{20:sep1}. Furthermore, $a$ is separable over $L'$, as $L/K$ is separable, so $\mip{a}{L}$ has no repeated roots in $\kk = \overline{L}$.
By \cref{20:compsep}, we have that $L'(a)/K$ is separable, so $a$ is separable over $K$.
\end{proof}
\begin{rem}
  Let $L/K$ be a separable field extension. Then
  \[
  L\fsep := \{ a\in L\ssp a\text{ is separable over }K\}
  \]
  is a subfield of $L/K$. The degree $[L\fsep:K]$ is the \toidx{separability degree} of $L$ over $K$.
\end{rem}
\section{Inseparable Field Extensions}
\begin{defn}
  Let $R$ be a commutative ring. Let \[f = \sum_{i=0}^n a_iX^i\in R[X]\] with $n:= \deg f \geq 0$. Set \[ \sum_{i=1}^{n}i(\cdot a_iX^{i-1}),\]
  which is called the \toidx{formal derivative} of $f$.
\end{defn}
\begin{nrem}
  In this section, we often use the $\zz$-algebra structure of $R$, where
  \[
  i\cdot a:= \sum_{j=1}^i a.
  \]
\end{nrem}
\begin{bsp}
  Assume $m=\rchar R>0$. For $f=X^m\in R[X]$, we have
  \[
  f' = m\cdot X = 0,
  \]
  as $m\cdot 1=0$.
\end{bsp}
\begin{lem}
  For $f,g\in R[X]$, we have
  \begin{enumerate}
    \item $\deg f'< \deg f$, for $\deg f >0$.
    \item $f'=0$, for $\deg f = 0$.
    \item $(f+g)' = f'+g'$.
    \item $(af)' = af'$, for all $a\in R$.
    \item $(fg)' = f'g+fg'$.
  \end{enumerate}
\end{lem}

\begin{lem}\label{20:dercalc}
  Let $f = (X-a)^mg\in R[X]$, with $a\in R$, $m\geq 1$. Then
  \[
  f' = (X-a)^{m-1}\cdot \left( mg + (X-a)g'\right).
  \]
\end{lem}
\begin{defn}
  Let $R$ be a commutative ring. Assume $p:= \rchar R$ is a prime number. In this case, we call
  \[
  F:R\to R, a\mapsto a^p:= \underbrace{a\hdots a}_{p\times}
  \]
  the \toidx{Frobenius Endomorphism} of $R$.
\end{defn}
\begin{lem}\label{20:frob}
  \begin{enumerate}
    \item The Frobenius Endomorphism is a ring homomorphism.
    \item $R^p := \{ a^p \ssp a \in R\}$ is a subring of $R$.
    \item If $R$ is an integral domain, then $R\cong R^p$.
    \item Assume $R$ is a finite field. Then $R = R^p$.
    %added later
    \item Let $\fp$ be the finite field with $p$ elements. Then $F=\id_{\fp}$.
  \end{enumerate}
\end{lem}

\begin{proof}
  \begin{enumerate}
    \item This follows from the fact the in characteristic $p$, we have $(a+b)^p = a^p+ b^p$.
    \item We have $\im F = R^p$, and images of ring homomorphisms are subrings.
    \item As $R$ is an integral domain, we have $a^p =0 \iff a =0$. So $\ker F = 0$.
    \item As $R$ is finite and $R^p\sse R$, the claim follows.
    \item We know that the additive group of $\fp$ is cyclic, given by $\left(\fp,+\right) = \langle 1\ssp p\cdot 1 = 0\rangle$. As $F$ is a ring homomorphism, $F$ is uniquely determined by its value on $1$, which is $1$. So $F = \id_{\fp}$ follows.
  \end{enumerate}
\end{proof}

\begin{thm}\label{20:sepcond}
  Let $f\in K[X]$ be irreducible. Then the following are equivalent:
  \begin{enumerate}
    \item $f$ is inseparable.
    \item $f'=0$.
    \item It holds that $p:= \rchar K >0$ and there is a separable irreducible polynomial $g \in K[X]$ and a $e>0$ such that
    \[
    f = g\left(X^{p^e}\right).
    \]
  \end{enumerate}
\end{thm}

\begin{proof}
  i) $\implies$ ii): Let $a\in \kk$ be a multiple root of $f$, i.e. $f = (X-a)^m\cdot g$ with $m\geq 2$ and $g\in \kk[X]$. By \cref{20:dercalc}, we have $f'(a)=0$. As $f$ is irreducible, we have that $f = \mip{a}{K}$, and hence in particular $f\divd f'$. But as $\deg f'\leq \deg f$, $f'=0$ follows.\par
  ii)  $\implies$ iii): Let $f = a_0+\hdots +a_nX^n$, with $n:= \deg f \geq 1$. Then $f' = a_1 +\hdots na_nX^{n-1}=0$. So $i\cdot a_i = 0$ for all $1\leq i \leq n$. Hence $p:= \rchar K >0$, and $a_i=0$ or $i\equiv p$ for all $1\leq i\leq p$. So
  \[
  f = \sum_{j=0}^{m} a_{j\cdot p}\left(X^p\right)^j
  \]
  for a $m\in \nn$. Consider now
  \[
  f_1 := \sum_{j=0}^m a_{j\cdot p}X^j.
  \]
  Then $f = f_1(X^p)$. We now show that $f_1$ is irreducible over $K$. For that, consider the maps
  \[
  F_1:K[X]\to K[X]^p,~ f\mapsto f^p,
  \]
  and
  \[
  F_2:K[X^p]\to K^p[X^p],~ \sum_{j\geq 0}b_j\left(X^p\right)^j\mapsto \sum_{j\geq 0}b_j^p\left(X^p\right)^j = \left(\sum_{j\geq 0}b_j X^j\right)^p.
  \]
  Both of them are isomorphisms, and hence $f_1 = F_1^{-1}\left(F_2(f)\right)$ is irreducible, as $f$ is. If $f_1$ is separable, the claim follows. Otherwise, the claim follows inductivley from i) $\implies$ ii).
  \item \coms Rest follows later.\come
  \end{proof}

  \coms this was added by me\come
  \section{Finite Fields}
  \subsection{Classification of Finite Fields}
  \begin{goal}
    We want to completely classify finite fields.
  \end{goal}
  \begin{prop}
    Let $\ff$ be a finite field of characteristic $\rchar \ff =p >0$. Then there is a $d\geq 1$ such that $\abs{\ff} = p^r$.
  \end{prop}
  \begin{proof}
    $\ff$ contains $\fp$ as a prime field. So we have the finite field extension $\fp \sse \ff$; i.e. we can regard $\ff$ as a finite-dimensional $\fp$-vector space, which implies $\abs{\ff} = p^{[\ff:\fp]}$.
  \end{proof}
  \begin{thm}\label{20: finite fields props}
  Let $p$ be prime, $d\geq 1$ and $q:= p^d$.
    \begin{enumerate}
      \item The polynomial $f:= X^q-X\in \fp[X]$ is separable, and the splitting field $\ff$ of $f$ is a field with precisley $q$ elements.
      \item Let $\ff$ be a field with exactly $q$ elements. Then $\ff$ is a splitting field for $X^q-X\in \fp[X]$.
    \end{enumerate}
  \end{thm}
  \begin{proof}
    \begin{enumerate}
      \item As $f' = qX^{q-1}-1 = -1 \neq 0$, we have by \cref{20:sepcond} that $f$ is separable. Hence $f$ factors completly in linear factors over $\ff$, with no repeated roots. As $\deg f = q$, this implies that the set $E$ of roots of $f$ in $\ff$ contains exactly $q$ elements. We first show that $E$ is itself a subfield of $\ff$:
      \begin{itemize}
        \item Let $a,b\in E$. Then
        \[
        (a-b)^q = a^q-b^q = a-b \implies (a-b)^q - (a-b) = 0 \iff a-b \in E.
        \]
        \item Let $a,b$ in $E$ with $b\neq 0$. Then
        \[
        (ab^{-1})^{q} = a^q\left(b^q\right)^{-1} = ab^{-1}.
        \]
      \end{itemize}
       So $E$ is indeed a subfield of $\ff$.\par
       %maybe this is easier, by just showing that $E$ is a linear subspace
       As $\ff$ is generated by the elements of $E$, the smalles subfield of $\ff$ that contains all elements of $E$ is $\ff$ itself. So $E=\ff$ follows, and $\abs{\ff} = q$.
       \item Let $\ff$ be a field with exactly $q$ elements. Then $\unts{\ff}$ is cyclic by \cref{8:cycunits}. Hence the order of every element $a\in \unts{\ff}$ divides $q-1 = \abs{\unts{\ff}}$, so
       \[
        a\neq 0\implies a^{q-1} = 1  \implies a^p - a = 0,
        \]
        and for $0$, we have $0^q-0 = 0$. So every element of $\ff$ is a root of $f$, so $\ff$ is a splitting field for $f$.
    \end{enumerate}
  \end{proof}
  Combining these results, we arrive at the classifcation of finite fields.
  \begin{thm}
    There is a bijection
    \[
    \begin{tikzcd}
    \lset \ff \;\middle|\;
    \begin{tabular}{@{}l@{}}
      $\ff$ finite field\\
      (up to isomorphism of fields)
     \end{tabular}
     \rset \ar{r}&
     \lset p^r\ssp p \text{ prime }, r\geq 1 \rset \\
     \ff \ar[mapsto]{r} &
     \abs{\ff}
     \end{tikzcd}
     \]
  \end{thm}
  \begin{proof}
    The preceeding \cref{20: finite fields props} implies already that evey field of characteristic $p$ is a splitting field, and that it is possible to construct these for every $r$. So by the uniqueness of splitting fields up to $\fp$-isomorphism (\cref{19: props of splitting fields}), the claim follows.
  \end{proof}
\subsection{Finite field extensions of finite fields}
\subsection{Frobenius homomorphism for finite fields }
  \begin{thm}
    Let $q := p^d$ for a prime number $p$ and a $d\geq 1$. Set $L:= \ff_{q}$ and $K:= \fp$. Then $\gal{L/K}$ is cylic, generated by the Frobenius homomorphism.
  \end{thm}
  \begin{proof}
    On a finite field, the Frobenius homomorphism $F$ is an $\fp$-isomorphism (\cref{20:frob}), so $F\in \gal(L/K)=:G$. Let $e:= \abs{G}$. Then $F^e = \id$, so $X^{p^e}=x$ for all $x\in L$. So $x^{p^e-1}-x$ has exactly $q=p^d$ roots in $L$. So $d\leq e$.
    By \cref{20: upper bound for galois group}, we have
    \[
    \abs{G} \leq [L:K] = d \leq e = \ord F.
    \]
    But as $\ord{F}\divd \ord{G}$, we have $\langle F\rangle = G$.
  \end{proof}
\lec
