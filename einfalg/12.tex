\subsection{Ring homomorphisms and Ideals}
\begin{lem}\label{12:hom}
Let $\pphi:R\to S$ be a ring homomorphism, $I\subseteq \ker \pphi \subseteq R$ an ideal of $R$. Then there is a unique ring homomorphism $\pphi'$ such that the diagram
\begin{equation}\label{12:homdia}
\begin{tikzcd}
R\ar{r}[above]{\pphi}\ar{d}[left]{p}&S\\
R/I\ar[dashed]{ur}[below right]{\exists! \phi'}&
\end{tikzcd}
\end{equation}
commutes.
\end{lem}
\begin{proof}
As $I$ is an ideal, it is in particular a normal subgroup $I\nsg (R,+)$. So there is already a well-defined group homomorphism making \eqref{12:homdia} commutes,
\[
\pphi'(a+I):= \pphi(a)
\]
So all we have to check is that $\pphi'$ is a ring homomorphism. But this is inherited from $\pphi$ as well:
\[
\pphi'(1+I) = \pphi(1) = 1\qquad \text{and}\qquad \pphi'((a+I)(b+I)) = \pphi(ab) = \pphi(a)\pphi(b)=\pphi'(a+I)\pphi'(b+I)
\]
\end{proof}
\begin{cor}\label{12:1iso}
Let $\pphi:R\to S$ be a surjective ring homomorphism. Then there is a unique isomorphism
\[
R/\ker \pphi \cong S
\]
\end{cor}
\begin{proof}
  This follows from \cref{12:hom}, as the induced group homomorphism for $R/I$ is bijective, and bijective ring homomorphisms are ring isomorphisms.
\end{proof}

\begin{lem}\label{12:idealcor}
  Let $f:R\to S$ be a ring homomorphism.
  \begin{enumerate}
    \item Let $I\subseteq S$ be an ideal. Then $f^{-1}(I)\subseteq R$ is an ideal of $R$.
    \item Assume that $f$ is surjective, and $I\subset R$ an ideal. Then $f(I)\subset S$ is an ideal.
    \item Assume that $f$ is surjective. Then there is a bijection
    \[
    \begin{tikzcd}
    \big \{ \text{Ideals of }S\big \} \ar[leftrightarrow]{d}&I\ar[mapsto]{d}\\
    \big \{ \text{Ideals}~J~\text{of }R\text{ such that }\ker f \subseteq J\big \}&f^{-1}(I)
    \end{tikzcd}.
    \]
  \end{enumerate}
\end{lem}
\begin{proof}
  This will be an exercise.
\end{proof}

\begin{cor}\label{12:subidealcor}
  Let $R$ be a ring, $I\subseteq R$ an ideal. The canonical projection $p:R\to R/I$  induces a bijection
  \[
  \begin{tikzcd}
  \big \{ \text{Ideals of }R/I\big \}\ar[leftrightarrow]{d}\\
  \big \{ \text{Ideals}~J~\text{of }R\text{ such that }I\subseteq J\big \}
  \end{tikzcd}
  \]
\end{cor}
\begin{proof}
  This follows from \cref{12:idealcor}, as the canonical projection is surjective, and $\ker \pphi = I$ holds.
\end{proof}
\begin{lem}[Noether]\label{12:1noether}
  Let $I\sse J \sse R$ be ideals of a ring $R$, and consider the ring homomorphism
  \[
  \begin{tikzcd}
  f: R\ar{r}&R/I\ar{r}{p_{J/I}}&(R/I)/(J/I).
  \end{tikzcd}
  \]
  Then
  \[
  \ker f = J\text{ and }(R/I)/(R/J)\cong R/J
  \]
\end{lem}
\begin{proof}
  Let $a\in J$. Then $a+I\in J/I$ and hence $a\in \ker f$. On the other hand, $a+I \in \ker(p{J/I})\implies a+I\in J/I$, and hence $a\in J$.
  The second claim follows from \cref{12:1iso}.
\end{proof}
\subsection{Characteristic of a Ring}
\begin{lem}
  Let $R$ be a ring.
  \begin{enumerate}
  \item Then there exists exactly one ring homomorphism
  \[
  \rho_R:\zz \to R.
  \]
  \item The image of $\rho_R$ is the intersection of all subrings, i.e.
  \[
  \im \rho_R = \bigcap_{\substack{S\sse R\\S\text{ subring}}} S.
  \]
  \end{enumerate}
\end{lem}
\begin{proof}
  Sometimes different.
\end{proof}
\begin{defn}
  Let $R$ be a ring. The \toidx{charactersitic} of $R$ is
  \[
  \rchar R:= \min \{n\in \nne\ssp n\cdot 1_R = 0\}
  \]
  if it exists and $\rchar R =0$ otherwise.
\end{defn}
\begin{cor}
  Let $R$ be a ring, with $\rchar R =n$. Then
  \[
  \ker(S_n) = (n)
  \]
  and hence
  \[
  \zz/(n)\cong \im \rho_R
  \]
\end{cor}
\subsection{Integral Domains and Principal Ideal Domains}
\begin{defn}
  A ring $R$ is a \toidx{integral domain}, if
  \begin{enumerate}
    \item $R$ is commutative.
    \item $R\neq 0$.
    \item $R$ has no zero-divisors.
  \end{enumerate}
\end{defn}

\begin{lem}\label{12:zmax}
  Let $R$ be an integral domain. Then $\rchar R=0$ or $\rchar R = p$ for a prime number $p$.
\end{lem}
\begin{proof}
  Assume $\rchar R = n\neq 0$. Let $n = pp'$ for $2\leq p,p'\in \nn$. As
  \[
  0 = n\cdot 1 = (p\cdot 1)\cdot (p'\cdot 1)
  \]
  $I$ being an integral domain implies $p\cdot 1=0$ or $p'\cdot 1=0$. But this is a contractition to the minimality of $n$.
\end{proof}

\begin{defn}
  A ring $R$ is called \toidx{principal ideal domain} if
  \begin{enumerate}
    \item $R$ is an integral domain.
    \item every ideal in $R$ is generated by a single element; i.e. for $I\subseteq R$ an ideal, there is an $a\in R$ such that $I=(a)$.
  \end{enumerate}
\end{defn}
\begin{bsp}\label{12:zprincipal}
  The integers $\zz$ are a prinicipal ideal domain, as every subgroup of $\zz$ is cyclic (And for every ring, every ideal is necessarily a subgroup of the additive group)
\end{bsp}


\subsection{Maximal Ideals}
\begin{defn}
  Let $R$ be a commutative ring. An ideal $I\subseteq R$ is called \emph{maximal}\index{ideal!maximal}, if $I\neq R$ and for every $I\subset J\subseteq R$, $J=R$ follows.
\end{defn}
\begin{bsp}
  Let $I\sse \zz$ be an ideal. Then $I$ is maximal if and only if $I=(p)$, for a prime number $p$.
\end{bsp}
\begin{proof}
  We know from the classification of cyclic groups that there is an ideal $(p)\subseteq (n)$ if and only if $n\divd p$.
\end{proof}
\begin{lem}
  Let $R$ be a commutative ring, and $I\subset R$ an ideal. Then the following are equivalent:
  \begin{enumerate}
    \item $I$ is a maximal ideal.
    \item $R/I$ is a field.
  \end{enumerate}
\end{lem}
\begin{proof}
  i) $\implies$ ii): Consider the ideal
  \[
  J := (I\cup \{a\}) = \{x+ra\ssp x\in I,r\in R\}.
  \]
  As $I$ is maximal and $I\subset J$, $J=R$ follows. Hence there are $x\in I,b\in R$ such that
  \[
  1 = x+ ab.
  \]
  As $a\ni I$, we get for the quotient ring
  \[
  0\neq (a+I)\qquad \text{and}\qquad (a+I)(b+I) = (ab+I)=(ab+x+I)=(1+I),
  \]
  so $a+I\in (R/I)^\times$. So every element of the form $a+I$ in $R/I$ is invertible, and hence $R/I$ is a field.\par
  ii) $\implies$ i): As $R/I$ is a field, the only ideals in $R/I$ are $R/I$ and $0$. Using the lifting of ideals from \cref{12:subidealcor}, we get that there are no ideals between $I$ and $R$, so $I$ is maximal.
\end{proof}
\begin{thm}[Krull\index{Krulls!Existence Theorem}]
  Let $0\neq R$ be a commutative ring. Then
  \begin{enumerate}
    \item There is a maximal ideal in $R$.
    \item Every ideal $I\neq R$ is contained in a maximal ideal.
  \end{enumerate}
\end{thm}
\begin{proof}
\begin{enumerate}
\item   Consider the set
  \[
  \mathcal{M}:= \{ I\subset R\text{ ideal}\}
  \]
  As $(0)\in \mathcal{M}$, $\mathcal{M}\neq \emptyset.$ We can define a partial order on $\mathcal{M}$ by setting
  \[
  I\leq J :\iff I\subseteq J
  \]
  Let $\mathcal{T}$ be any chain in $\mathcal{M}$. Consider the set
  \[
  \overline{I}_{\mathcal{T}}:= \bigcup_{I\in \mathcal{T}}I.
  \]
  Then $\mathcal{T}$ is also an ideal and as $1\ni \mathcal{T}$, $\mathcal{T} \in \mathcal{M}$. Now Zorns Lemma implies that $\mathcal{M}$ has a maximal element, as every chain $\mathcal{T}$ in $\mathcal{M}$ has an upper bound $\overline{I}_{\mathcal{T}}$.
  But (inclusion) maximal elements in $\mathcal{M}$ are precisley maximal ideals.
  \item Let $I\neq R$ be an ideal. Then $R/I\neq 0$. So by i), there is a maximal ideal in $R/I$. But by \cref{12:subidealcor}, this implies that there is a maximal ideal in $R$ containing $I$.
\end{enumerate}
\end{proof}
%by aaron, mark later, from atiyah
\begin{prop}
  \begin{enumerate}
    \item Let $A$ be a ring and $m\neq (1)$ an ideal of $A$ such that every $x\in A\setminus m$ is a unit in $A$. Then $A$ is a local ring, and $m$ its maximal ideal.
    \item Let $A$ be a ring and $m$ a maximal ideal of $A$ such that every element of $1+m$ is a unit in $A$. Then $A$ is a local ring.
  \end{enumerate}
\end{prop}
\begin{proof}
\begin{enumerate}
  \item Every ideal $I\neq (1)$ in $A$ contains at least a non-unit, so it contained in $m$. Hence $m$ is the only (possible) maximal ideal.
  \item Let $x\in A\setminus m$. As $m$ is maximal, the ideal generated by $x$ and $m$ is $(1)$, and hence there are $y\in A$ in $t\in m$ such that $xy + t = 1$; hence $xy = 1-t\in 1+m$, and is therefore a unit. The claim follows by i).
\end{enumerate}
\end{proof}
\subsection{Prime Ideals}
\begin{defn}
  Let $R$ be a commutative ring. An ideal $I\subseteq R$ is a \emph{prime ideal}\index{ideal!prime}, if $I\neq R$ and for all $a,b\in R$ with $ab\in I$, $a\in I$ or $b\in I$ follows.\\
  The set of all prime ideals
  \[
  \spec(R):= \{ \text{Prime Ideals in }R\}
  \]
  is the \toidx{spectrum} of $R$ and the set
  \[
  \maxspec(R):= \{ \text{Maximal Ideals in }R\}
  \]
  is the \toidx{maximal spectrum} of $R$.
\end{defn}
\lec
