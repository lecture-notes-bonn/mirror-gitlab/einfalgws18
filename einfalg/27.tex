Let $n\geq 1$, and $K$ be a field. We assume that if $p:= \rchar K >0$, $p\ndivd n$.

\begin{defn}
  Let $\pp_n := \{ z_1,\hdots, z_{\pphi(n)}\}$ be the set of primitive $n$-th roots of unity. We call
  \[
  \Phi_n(K) := \prod_{i=1}^n \left(X-z_i\right) \in \overline{K}[X]
  \]
  the $n$-th \toidx{cyclotomic polynomial}.
\end{defn}
\begin{rem}
  The $n$-th cyclotomic field $\zeronk$ is a splitting field of $\Phi_n(K)$ over $K$.
\end{rem}

\begin{bsp}
\leavevmode
  \begin{enumerate}
    \item For $n=1$, we have $\Phi_1(K) = X-1$.
    \item For $p$ prime, we have $X^n-1 = \Phi_1(K)\cdot \Phi_p(K)$, as every $p$-th root of unity except $1$ is primitive. So
    \[\Phi_p(K) = X^{p-1}+\hdots+X+1\].
  \end{enumerate}
\end{bsp}

\begin{thm}
  \leavevmode
  \begin{enumerate}
    \item The coefficients of $\Phi_n(K)$ are in $K$, i.e. $\Phi_n(K) \in K[X]$. Furthermore, $\Phi_n(K)$ is monic, separable and of degree $\pphi(n)$.
    \item For $K=\qq$, $\Phi_n(\qq)$ is irreducible and has integer coefficients (so in fact $\Phi_n(K)\in \zz[X]$).
    \item We have $X^n-1 = \prod_{d\divd n}\Phi_d(K)$.
    \item For any field $K$, we can construc $\Phi_n(\qq)$ be using the unique ring homomorphism $\rho_: \zz \to K$, i.e. $\Phi_n(K) \left(\rho_K\left(\Phi_n(\qq)\right)\right)$.
  \end{enumerate}
\end{thm}

\begin{proof}
We denote by $z_1,\hdots,z_{\pphi(n)}$ the primitive $n$-th roots of unity.
  \begin{enumerate}
    \item It is clear that $\Phi_n(K)$ is monic and of degree $\pphi(n)$, by multiplying out the linear factors.\\
    Now let $L:= K(z_1,\hdots,z_n) = K(z_1)$ be the splitting field of $\Phi_n(K)$ over $K$. We can then regard $\Phi_n(K)$ as an element of $L[X]$. \\
    Let now $\phi\in \gal(L/K)$. Then $\phi(\pp_n)\sse \pp_n:$ We have
    \[
    \phi(z_i)^n = \phi(z_i^n) = 1
    \]
    and hence $\phi(z_i)$ is also a $n$-th root of unity. Assume now that there is a $1\leq m\leq n$ such that $f(z_i^m) = 1$. Then by Injectivity of $\phi$, we have $z_i^m = 1$, which only holds if $m=n$.\\
    By linearity, we get $\phi\left(\Phi_n\right) = \phi\left(\Phi_n\right)$, so $\phi \in L^{\gal(L/K)}[X] = K[X]$, as $L/K$ is a Galois extension \cref{23: fundamental theorem galois}.
    \item Let $z$ be a primitve $n$-th root of unity. We have $\mip{z}{\qq}\divd \Phi_n(\qq)$, as they have $z$ as a common root. By \cref{26: degree of cyclotomic extension}, we have
    \[
    [\qq(z):\qq] = \deg \mip{z}{\qq} = \pphi(n).
    \]
    So $\mip{z}{\qq} = \Phi_n(\qq)$, which implies in particular that $\Phi_n(\qq)$ is irreducible over $\qq$.\\
    As $\Phi_n(\qq)$ and $X^n-1\in \qq[X]$ they have common roots, so there is a $h\in \qq[X]$ such that
    \[
    X^n-1 = \Phi_n(\qq)\cdot h.
    \]
    Now by \cref{26: coefficients in z} we have $\Phi_n(\qq),h\in \zz[X]$. As $\Phi_n(X)$ is monic, it is in particular a primitive polynomial in $\zz[X]$. So by \hyperref[17:gauss]{Gauss' theorem}, $\Phi_n(\qq)$ is also irreducible in $\zz[X]$.
    \item We have, using the notation $\pp_d(n)$ for the set of primitive $n$-th roots of unity,
    \[
      \zeronk = \dot{\bigcup_{d\divd n}}~\pp_d(K),
    \]
    which already implies
    \begin{equation}\label{27: cyclotomic for product}
    X^n-1 = \prod_{d\divd n}\left(\prod_{z\in \pp_d(K)}\left(X-z\right)\right).
    \end{equation}
    \item Consider the induced ring homomorphism\footnote{This should just be the natural $\zz$-algebra structure on the given ring $K$.}
    \[
    \eta: \begin{tikzcd}[sep = small, cramped]\zz[X]\ar{r}&K[X],&&\sum a_iX^i \ar[mapsto]{r}&\sum \rho_K(a_i)X^i\end{tikzcd}.
    \]
    We will show the claim by induction on $n$: For $n= 1$, we have
    \[\Phi_1(K)=X-1 = \eta\left(\Phi_1(\qq)\right).\]
    For $n\geq 2$, $\eta$ being a ring homomorphism implies first of all
    \[
    X^n-1 = \eta\left( X^n-1\right).\]
    Using now the decomposition in $\qq$ of $X$ as in \eqref{27: cyclotomic for product}, we get
    \begin{align*}
      X^n-1 &= \eta\left(\Phi_n(\qq)\cdot \prod_{\substack{d\divd n\\ d<n}} \Phi_d(\qq)\right)\\
            &= \eta\left(\Phi_n(\qq)\right) \cdot \prod_{\substack{d\divd n\\ d<n}} \Phi_d(K)
    \end{align*}
    But there is also the direct decomposition of \eqref{27: cyclotomic for product} in $K$:
    \[
    X^n-1 = \Phi_n(K) \cdot \prod_{\substack{d\divd n\\ d<n}} \Phi_d(K).
    \]
    So$
    \Phi_n(K) = \eta \left(\Phi_n(\qq)\right)
    $
    follows.
  \end{enumerate}
\end{proof}

\begin{cor}
  \leavevmode
  \begin{enumerate}
    \item $\gal\left(\Phi_n(\qq))\right) \cong \unts{\zz/n\zz}$.
    \item For every pair $(z,z')$ of primitive $n$-th roots of unity there is exactly one $\phi \in \gal\left(\Phi_n(\qq))\right)$ such that $\phi(z) = z'$.
  \end{enumerate}
\end{cor}

\section{Solvability by Radicals}
\begin{lem}\label{27: solvable basic lemma}
  Let $K$ be a field. Assume $\zeronk \sse K$. Let $f = X^n-b \in K[X]$, and $L$ be a splitting field of $f$ over $K$. Then $\gal(L/K)$ is cyclic and $\abs{\gal(L/K)}\divd n$.
\end{lem}

\begin{proof}
If $b = 0$, then the claim is trivial. So assume $b\neq 0$. Denote by $\zeronk := \{z_1,\hdots,z_n\}$ the set of $n$-th roots of unity.  Let $a\in L$ be a root of $f$.
Then
\[
\lset \text{roots of }a \text{ in }L\rset = \lset az_1,\hdots,az_n\rset.
\]
So $L = K(az_1,\hdots,az_n) = K(a)$ (as $z_1,\hdots,z_n \in K$ by assumption). \\
Let $\phi_1,\phi_2\in \gal(L/K)$ with $\phi_1(a) = az_i$ and $\phi_2(a)  = az_j$, for some $z_i,z_j \in \zeronk$. Then by lifiting properties, $\phi_1$ and $\phi_2$ are uniquely determined by this property. As we also have $\left(\phi_2\circ \phi_1a\right) = \phi_2(a)z_i az_iz_j$, we get an injective group homomorphism :
\[
\begin{tikzcd}[sep = small, cramped]
  \gal(L/K)\ar{r}&\zeronk \cong \zz/n\zz \\
  \phi \ar[mapsto]{r}&\phi(a)/a
\end{tikzcd}
\]
So we can identify $\gal(L/K)$ with a subgroup of $\zz/n\zz$, so it is cyclic, and by Lagrange, we have $\abs{\gal(L/K)} \divd n$.
\end{proof}
\begin{lem}
  Assume $\zeronk \sse K$ for a cyclic field extension $L/K$ of degree $n$. Then there is a primitive element $a\in L$ such that $L = K(a)$ and $a^n \in K$.
\end{lem}

\begin{proof}
  Let $f$ be a generator of $\gal(L/K)$, i.e. $\langle f\rangle = \gal(L/K)$. Let $z\in \zeronk$ be a $n$-th root of unity.\\
  We can regard any $\phi \in \gal(L/K)$ as a character $\unts{L} \to \unts{L}$, by restricting it to $\unts{L}$ (and using that the kernel of field-automorphism is always trivial).\\
  So by \coms 4.14\come, we have
  \[
  (z,x):= x + z f(x) + \hdots + z^{n-1} f^{n-1}(x) \neq 0,
  \]
  as $z^i\neq 0$ for all $i$. Applying\footnote{We had to use $f$ here, because this guarentees that $1,f,\hdots,f^{n-1}$ are different characters.} $f$ gives $f\left((z,x)\right) = z^{-1}(z,x)$. So
  \begin{align*}
    f\left((z,x)\right)^n &= \left(f(z,x)\right)^n\\
    &= \left(z^{-1}\right)^n (z,x)\\
    &= (z,x)^n.
  \end{align*}
  Extending this to the whole Galois group, we get $\phi\left((z,x)^n\right) = (z,x)^n$ for all $\phi \in \gal(L/K)$. Hence $(z,x)^n$ is an element of $L^{\gal(L/K)} = K$ (where the last equality follows from \cref{23: fundamental theorem galois}). \\
  As the $f^j((z,x))$ are different for all $1\leq j \leq n$, we have by the lifting properties that the degree of the minimal polynomial $\mip{(z,x)}{K}$ is at least $n$. As $(z,x)$ is an element of $L$, $L = K\left((z,x)\right)$ follows.
\end{proof}

The last two theorems of this semester are about solvability of polynomials using radicals. Recall the following defintions, from chapter 1 of the lecture:
\begin{ndefn}\leavevmode
  \begin{enumerate}
    \item Let $L/K$ be a field extension. We say $L/K$ is a \toidx{radical extension}, if
    \begin{enumerate}[label = \alph*)]
      \item there are $x_1,\hdots,x_n\in L$ such that $L = K(x_1,\hdots,X_n)$; and
      \item for each of the $x_i$, there is an $r_i\geq 1$, such that $x_1^{r_1}\in K$, and $x_i^{r_i}\in K(x_1,\hdots,x_{i-1})$.
    \end{enumerate}
    In this case, the elements in $L$ are called \toidx{radicals}.
    \item Let $f\in K[X]$ be a polynomial. We say that $f$ is \toidx{solvable by radicals}, if there is a radical extension $L/K$ such that $f$ has a root in $L$.
  \end{enumerate}
\end{ndefn}
\begin{thm}[\glqq Satz 1\grqq]\label{27: solvable1}
  Let $K$ be a field, and $f\in K[X]$ an irreducible and separable polynomial, such that
  \begin{itemize}
    \item $\gal(L/K)$ is solvable; and
    \item $\rchar K \ndivd \abs{\gal(f)}$.
  \end{itemize}
  Then $f$ is solvable by radicals, and all roots of $f$ in $\overline{K}$ are radicals over $K$.
\end{thm}
\begin{thm}[\glqq Satz 2\grqq]\label{27: solvable2}
  Let $K$ be a field with characteristic $\rchar K = 0$ and $f\in K[X]$ an irreducible polynomial, such that $f$ is solvable by radicals. Then $\gal(f)$ is a solvable group.
\end{thm}

\begin{cor}
  Let $K$ be a field of characteristic $\rchar K = 0$. Then the following are equivalent:
  \begin{enumerate}
    \item $f$ is solvable by radicals.
     \item $\gal(f)$ is a solvable group.
  \end{enumerate}
\end{cor}
We need the following theorem concerning the refinment of solvable groups, which was proven on \coms an exercise sheet\come:
\begin{nthm}
 Let $1\neq G$ be a solvable group, and $N\nsg G$ a normal subgroup. Then there is a normal resolution
 \[
 1 = N_0 \nsg N_1 \nsg \hdots \nsg N_t = G
 \]
 such that
 \begin{enumerate}
   \item the quotient groups $N_i/N_{i-1}$ are cyclic of order $p_i$ (for all $1\leq i \leq t$); and
   \item $N\in \lset N_0,\hdots,N_t\rset$.
 \end{enumerate}
 \end{nthm}
\begin{proof}[Proof of \cref{27: solvable1}]
  Let $L$ be a splitting field of $f$ over $K$. As $G:= \gal(f) = \gal(L/K)$ is a solvable group, there is a normal series
  \[
  1 = N_0 \nsg N_1 \nsg \hdots \nsg N_t = G,
  \]
  such that $N_i / N_{i-1}$ is cyclic of prime order for every $1\leq i \leq t$. By \cref{24: metacyclic iff solvable}, this corresponds to a chain
  \[
  K = Z_0 \sse Z_1 \sse \hdots \sse Z_t = L \]
  such that $Z_i/Z_{i-1}$ is cyclic of prime degree $p_i$. Let $n:= [L:K]$ and $p:= \rchar K$. Then $n = \ord{G} = p_1 \hdots p_t$, so by assumption, $p_i \neq p$ for all $1\leq i \leq t.$\\
  Define
  \[
  K' := K\left(\zeronk\right) \qquad \text{and}\qquad L':= K'.L \sse \overline{K}.
  \]
  Then $K'$ contains the $p_i$-th roots of unity for all $1\leq i \leq t$. \par
  Consider now the chain
  \[
  K' = K'.Z_0 \sse K'.Z_1\sse \hdots \sse K'.Z_t = L'.
  \]
  Then by \cref{24: translation lemma}, the field extensions $K'.Z_i / K'.Z_{i-1}$ are cyclic for all $1\leq i \leq t$. So by \cref{27: solvable basic lemma}, there are $a_i \in K'.Z_i$ and $n_1 \geq 1$ such that
  \[
  K'.Z_i = \left(K'.Z_{i-1}\right)(a_i) \qquad \text{and} \qquad a_i^{n_i} \in K'.Z_{i-1}
  \]
  for all $1\leq i \leq t$. So $L'/K'$ is a radical extension. \\
  As $K'/K$ is also a radical extension (again by \cref{27: solvable basic lemma}), $L'/K$ is also a radical extension. As $f$ has a root in $L'$, this shows that $f$ is solvable by radicals. As $L\sse L'$, we have that in fact \textit{all} roots of $f$ are in $L'$.
\end{proof}
\lec
