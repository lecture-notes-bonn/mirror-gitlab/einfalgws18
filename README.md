These are my personal lecture notes for the lecture _Introduction to Algebra_ held by Prof. Dr. Jan Schröer at the University of Bonn in the winter term 2018/19. They can be found [here](https://pankratius.gitlab.io/einfalgws18/EinfAlg_WS18.pdf)

### ToDo:
* Fix numbering. Possible options:
⋅⋅* Add separate numbering for the parts added later.
⋅⋅* Do some kin of tag-system, where the original parts are marked by separate tags on the page margin.
* Reference to theorems by names. The current hyperref-solution is highly unflexible.
